import {HttpErrorResponse} from '@angular/common/http';
import {Component, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {FullCalendarComponent} from '@fullcalendar/angular';
import bootstrapPlugin from '@fullcalendar/bootstrap';
import {Calendar} from '@fullcalendar/core';
import timeGridPlugin from '@fullcalendar/timegrid';
import * as moment from 'moment';
import {MessageService} from 'primeng/api';
import {BusChauffeur} from '../modals/bus-chauffeur';
import {EffectieveDienst} from '../modals/effectieve-dienst';
import {EffectieveDienstService} from '../services/effectieve-dienst.service';
import {FeestdagenService} from '../services/feestdagen.service';

@Component({
    selector: 'app-dienst-kalender',
    templateUrl: './dienst-kalender.component.html',
    styleUrls: ['./dienst-kalender.component.scss'],
})
export class dienstKalenderComponent {
    @ViewChild('calendar', {static: true})
    calendarComponent: FullCalendarComponent;
    public api: Calendar;
    @Input() busChauffeur: BusChauffeur;
    @Input() jumpToDate?: Date;
    @Output() kalenderDatum = new EventEmitter<Date>();
    public effectieveDienstenVoorDezeWeek: EffectieveDienst[] = [];
    calendarPlugins = [timeGridPlugin, bootstrapPlugin];
    getCustomButtons = {
        vorige: {
            bootstrapFontAwesome: 'fa-arrow-left',
            click: this.veranderWeekPrev.bind(this),
        },
        volgende: {
            bootstrapFontAwesome: 'fa-arrow-right',
            click: this.veranderWeekNext.bind(this),
        },
        vandaag: {
            text: 'Vandaag',
            click: this.veranderWeekVandaag.bind(this),
        },
        bewerkWeek: {
            text: 'Bewerk deze week',
            click: this.bewerkWeek.bind(this),
        },
    };

    public eventData = [];
    public recurringEvents = undefined;
    public feestdagen = undefined;
    constructor(
        private messageService: MessageService,
        private router: Router,
        private feestdagenService: FeestdagenService,
        private effectieveDienstService: EffectieveDienstService,
    ) {}

    ngAfterViewInit() {
        this.api = this.calendarComponent.getApi();
        if (this.jumpToDate != undefined) {
            this.api.gotoDate(moment().isoWeekYear(moment(this.jumpToDate).year()).isoWeek(moment(this.jumpToDate).week()).startOf('week').toDate());
        } else {
            this.api.gotoDate(moment().isoWeekYear(moment().year()).isoWeek(moment().week()).startOf('week').toDate());
        }
        this.kalenderDatum.emit(this.getCurrentDate());
        this.loadCalenderVoorWeek();
    }

    loadCalenderVoorWeek() {
        this.effectieveDienstService
            .getEffectieveDiensten$(this.getCurrentYear().toString(), this.getCurrentWeekNumber().toString(), this.busChauffeur)
            .subscribe(
                val => {
                    if (val) {
                        this.eventData = [];
                        this.loadFeestdagen();
                        if (val.length === 0) {
                            this.effectieveDienstenVoorDezeWeek = [];
                            this.loadRecurrentEvents();
                        } else {
                            this.effectieveDienstenVoorDezeWeek = val;
                            this.loadEffectieveDiensten();
                        }
                    }
                },
                (error: HttpErrorResponse) => {
                    if (error.error instanceof ProgressEvent) {
                        this.messageService.add({severity: 'error', summary: 'De databank is onbereikbaar!'});
                    } else {
                        console.log(error);
                        this.messageService.add({severity: 'error', summary: error.error, life: 10000});
                    }
                },
            );
    }

    loadRecurrentEvents() {
        if (this.recurringEvents === undefined) {
            this.recurringEvents = [];
            this.busChauffeur.diensten.forEach(dienst => {
                var convertedStartDag = dienst.startDag == 7 ? 0 : dienst.startDag;
                var convertedEindDag = dienst.eindDag == 7 ? 0 : dienst.eindDag;
                if (convertedStartDag !== convertedEindDag) {
                    this.recurringEvents.push(
                        {
                            title: dienst.naam,
                            daysOfWeek: [convertedStartDag],
                            startTime: dienst.startUur,
                            endTime: '24:00',
                            url: `../dienst-overzicht`,
                            color: '#0288D1',
                        },
                        {
                            title: dienst.naam,
                            daysOfWeek: [convertedEindDag],
                            startTime: '0:00',
                            endTime: dienst.eindUur,
                            url: `../dienst-overzicht`,
                            color: '#0288D1',
                        },
                    );
                } else {
                    this.recurringEvents.push({
                        title: dienst.naam,
                        daysOfWeek: [convertedStartDag],
                        startTime: dienst.startUur,
                        endTime: dienst.eindUur,
                        url: `../dienst-overzicht`,
                        color: '#0288D1',
                    });
                }
            });
        }
        this.recurringEvents.forEach(t => this.eventData.push(t));
    }

    loadFeestdagen() {
        if (this.feestdagen === undefined) {
            this.feestdagen = [];
            this.feestdagenService.getAllFeestdagen$().subscribe(val => {
                if (val) {
                    val.forEach(fd => {
                        let event = {
                            title: fd.naam,
                            date: this.getDateForInput(fd.dag),
                            color: '#21b552',
                            textColor: '#000',
                            url: '/feestdagen-aanpassen',
                        };
                        this.feestdagen.push(event);
                        this.eventData.push(event);
                    });
                    let verjaardag = new Date();
                    verjaardag.setDate(this.busChauffeur.geboorteDatum.getDate());
                    verjaardag.setMonth(this.busChauffeur.geboorteDatum.getMonth());
                    let verjaardagsEvent = {
                        title: `Verjaardag ${this.busChauffeur.voornaam} ${this.busChauffeur.achternaam}`,
                        date: this.getDateForInput(verjaardag),
                        color: '#21b552',
                        textColor: '#000',
                    };
                    this.feestdagen.push(verjaardagsEvent);
                    this.eventData.push(verjaardagsEvent);
                }
            });
        } else {
            this.feestdagen.forEach(t => this.eventData.push(t));
        }
    }

    loadEffectieveDiensten() {
        this.effectieveDienstenVoorDezeWeek.forEach(ed => {
            if (ed.type == 1) {
                this.eventData.push({
                    title: ed.naam,
                    start: this.getDateForInputMetTijd(ed.start),
                    end: this.getDateForInputMetTijd(ed.einde),
                    color: '#FBC02D',
                    textColor: '#000',
                });
            } else {
                this.eventData.push({
                    title: ed.naam,
                    allDay: true,
                    start: this.getDateForInput(ed.start),
                    setAllDay: true,
                    color: '#FBC02D',
                    textColor: '#000',
                });
            }
        });
    }

    public veranderWeekNext() {
        this.api.next();
        this.kalenderDatum.emit(this.getCurrentDate());
        this.loadCalenderVoorWeek();
    }

    public veranderWeekVandaag() {
        this.api.gotoDate(moment().isoWeekYear(moment().year()).isoWeek(moment().week()).startOf('week').toDate());
        this.kalenderDatum.emit(this.getCurrentDate());
        this.loadCalenderVoorWeek();
    }

    public veranderWeekPrev() {
        this.api.prev();
        this.kalenderDatum.emit(this.getCurrentDate());
        this.loadCalenderVoorWeek();
    }

    getHeader() {
        return {
            left: 'bewerkWeek',
            center: 'title',
            right: 'vandaag, vorige,volgende',
            // right: 'today prev,next'
        };
    }

    bewerkWeek() {
        var dienstenWerdenOmgezet = false;
        var teBewerkenDiensten = [];
        if (this.effectieveDienstenVoorDezeWeek.length == 0) {
            dienstenWerdenOmgezet = true;
            teBewerkenDiensten = this.busChauffeur.diensten.map(dienst => dienst.toEffectieveDienst(this.getCurrentDate(), this.busChauffeur));
        } else {
            this.effectieveDienstenVoorDezeWeek.forEach(ed => {
                if (ed.gerelateerdeDienst !== null) {
                    if (ed.start < new Date(ed.gerelateerdeDienst.start)) {
                        ed.einde = new Date(ed.gerelateerdeDienst.einde);
                        teBewerkenDiensten.push(ed);
                    }
                } else {
                    teBewerkenDiensten.push(ed);
                }
            });
        }
        this.router.navigate([`../../effectieve-week-wijzigen/${this.getCurrentYear()}/${this.getCurrentWeekNumber()}/${this.busChauffeur.id}`], {
            state: {effectieveDiensten: teBewerkenDiensten, dienstenWerdenOmgezet: dienstenWerdenOmgezet, maandag: this.getCurrentDate()},
        });
    }

    getCurrentDate() {
        return this.api.getDate();
    }

    getCurrentYear(): number {
        return this.getCurrentDate().getFullYear();
    }

    getCurrentWeekNumber() {
        return moment(this.getCurrentDate()).week();
    }

    getDateForInput(date: Date): string {
        let uitvoer = '';
        uitvoer += date.getFullYear() + '-';
        if ((date.getMonth() + 1).toString().length === 1) {
            uitvoer += '0' + (date.getMonth() + 1) + '-';
        } else {
            uitvoer += date.getMonth() + 1 + '-';
        }
        if (date.getDate().toString().length === 1) {
            uitvoer += '0' + date.getDate();
        } else {
            uitvoer += date.getDate();
        }
        return uitvoer;
    }

    getDateForInputMetTijd(date: Date): string {
        let uitvoer = '';
        uitvoer += date.getFullYear() + '-';
        if ((date.getMonth() + 1).toString().length === 1) {
            uitvoer += '0' + (date.getMonth() + 1) + '-';
        } else {
            uitvoer += date.getMonth() + 1 + '-';
        }
        if (date.getDate().toString().length === 1) {
            uitvoer += '0' + date.getDate();
        } else {
            uitvoer += date.getDate();
        }
        uitvoer += 'T' + date.toTimeString().slice(0, 5);
        return uitvoer;
    }
}
