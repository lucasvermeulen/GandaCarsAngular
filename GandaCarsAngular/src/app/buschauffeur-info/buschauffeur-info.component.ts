import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import * as moment from 'moment';
import {DialogService} from 'primeng/dynamicdialog';
import {DienstTypeOverzichtTabelComponent} from '../dienst-type-overzicht-tabel/dienst-type-overzicht-tabel.component';
import {BusChauffeur} from '../modals/bus-chauffeur';
import {Stelsels} from '../modals/stelsels.enum';
import {tabelData} from '../modals/tabel-data';
@Component({
    selector: 'app-buschauffeur-info',
    templateUrl: './buschauffeur-info.component.html',
    styleUrls: ['./buschauffeur-info.component.scss'],
})
export class BuschauffeurInfoComponent implements OnInit {
    public busChauffeur: BusChauffeur;
    public stelsels = Stelsels;
    public jaarlijksVerlofTabel = [];
    public ziekteTabel = [];
    public oudersschapVerlofTabel = [];
    private jumpToJaar = null;
    private jumpToWeek = null;
    public jumpToDate: Date;
    @Input()
    public kalenderDatum: Date = new Date();

    constructor(public dialogService: DialogService, private route: ActivatedRoute, private router: Router) {
        this.route.data.subscribe(data => {
            this.busChauffeur = data.busChauffeur;
        });
    }

    ngOnInit() {
        this.route.queryParams.subscribe(params => {
            this.jumpToJaar = params.jaar;
            this.jumpToWeek = params.week;
        });
        this.setJumpToDate();
        let jaren = Array.from(new Set(this.busChauffeur.effectieveDiensten.map(x => x.start.getFullYear())));
        jaren.forEach(jaar => {
            if (this.jaarlijksVerlofTabel.find(x => x.jaar == jaar) == undefined) {
                let data = new tabelData();
                data.jaar = jaar;
                this.jaarlijksVerlofTabel.push(data);
            }
            this.busChauffeur.effectieveDiensten
                .filter(x => x.start.getFullYear() == jaar && x.type == 5)
                .forEach(gevondenDienst => {
                    this.jaarlijksVerlofTabel.find(x => x.jaar == jaar).diensten.push(gevondenDienst);
                    this.jaarlijksVerlofTabel.find(x => x.jaar == jaar).aantal++;
                });
            if (this.ziekteTabel.find(x => x.jaar == jaar) == undefined) {
                let data = new tabelData();
                data.jaar = jaar;
                this.ziekteTabel.push(data);
            }
            this.busChauffeur.effectieveDiensten
                .filter(x => x.start.getFullYear() == jaar && x.type == 2)
                .forEach(gevondenDienst => {
                    this.ziekteTabel.find(x => x.jaar == jaar).diensten.push(gevondenDienst);
                    this.ziekteTabel.find(x => x.jaar == jaar).aantal++;
                });

            if (this.oudersschapVerlofTabel.find(x => x.jaar == jaar) == undefined) {
                let data = new tabelData();
                data.jaar = jaar;
                this.oudersschapVerlofTabel.push(data);
            }
            this.busChauffeur.effectieveDiensten
                .filter(x => x.start.getFullYear() == jaar && x.type == 3)
                .forEach(gevondenDienst => {
                    this.oudersschapVerlofTabel.find(x => x.jaar == jaar).diensten.push(gevondenDienst);
                    this.oudersschapVerlofTabel.find(x => x.jaar == jaar).aantal++;
                });
        });
    }

    setJumpToDate() {
        if (this.jumpToJaar != null && this.jumpToWeek != null) {
            this.jumpToDate = moment().day('Monday').week(this.jumpToWeek).year(this.jumpToJaar).toDate();
        }
    }

    jaarlijksVerlofOverzicht() {
        const ref = this.dialogService.open(DienstTypeOverzichtTabelComponent, {
            data: this.jaarlijksVerlofTabel,
            header: `Jaarlijks verlof van ${this.busChauffeur.voornaam} ${this.busChauffeur.achternaam}`,
            width: '70%',
        });
    }

    ziekteOverzicht() {
        const ref = this.dialogService.open(DienstTypeOverzichtTabelComponent, {
            data: this.ziekteTabel,
            header: `Ziekte overzicht van ${this.busChauffeur.voornaam} ${this.busChauffeur.achternaam}`,
            width: '70%',
        });
    }
    oudersschapVerlofOverzicht() {
        const ref = this.dialogService.open(DienstTypeOverzichtTabelComponent, {
            data: this.oudersschapVerlofTabel,
            header: `Ouderschapsverlof overzicht van ${this.busChauffeur.voornaam} ${this.busChauffeur.achternaam}`,
            width: '70%',
        });
    }

    aantalDagenJaarlijksVerlofTegoed(): number {
        var aantal = 0;
        if (this.jaarlijksVerlofTabel.find(x => x.jaar == new Date().getFullYear())) {
            aantal = this.jaarlijksVerlofTabel.find(x => x.jaar == new Date().getFullYear()).aantal;
        }
        return this.busChauffeur.aantalDagenJaarlijksVerlof - aantal;
    }

    huidigJaar(): number {
        return new Date().getFullYear();
    }

    msToTime(s: number = 0): string {
        const ms = s % 1000;
        s = (s - ms) / 1000;
        const secs = s % 60;
        s = (s - secs) / 60;
        const mins = s % 60;
        const hrs = (s - mins) / 60;
        let uitvoer = '';
        if (s >= 0) {
            if (hrs.toString().length < 2) {
                uitvoer = `0${hrs}:`;
            } else {
                uitvoer = `${hrs}:`;
            }
            if (mins.toString().length < 2) {
                uitvoer += `0${mins}`;
            } else {
                uitvoer += `${mins}`;
            }
        } else {
            if (hrs.toString().length < 3) {
                uitvoer = `-0${Math.abs(hrs)}:`;
            } else {
                uitvoer = `${hrs}:`;
            }
            if (Math.abs(mins).toString().length < 2) {
                uitvoer += `0${Math.abs(mins)}`;
            } else {
                uitvoer += `${Math.abs(mins)}`;
            }
        }
        return uitvoer;
    }
}
