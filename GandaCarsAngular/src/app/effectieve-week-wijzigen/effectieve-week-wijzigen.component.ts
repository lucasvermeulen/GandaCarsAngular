import {HttpErrorResponse} from '@angular/common/http';
import {Component, OnInit} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import * as _ from 'lodash';
import * as moment from 'moment';
import {MessageService} from 'primeng/api';
import {BusChauffeur} from '../modals/bus-chauffeur';
import {Dienst} from '../modals/dienst';
import {Diensttypes} from '../modals/diensttypes.enum';
import {EffectieveDienst} from '../modals/effectieve-dienst';
import {EffectieveOnderbreking} from '../modals/effectieve-onderbreking';
import {Onderbrekingstypes} from '../modals/onderbrekingstypes.enum';
import {DienstService} from '../services/dienst.service';
import {EffectieveDienstService} from '../services/effectieve-dienst.service';
@Component({
    selector: 'app-effectieve-week-wijzigen',
    templateUrl: './effectieve-week-wijzigen.component.html',
    styleUrls: ['./effectieve-week-wijzigen.component.scss'],
})
export class EffectieveWeekWijzigenComponent implements OnInit {
    public bewerkteEffectieveDiensten: EffectieveDienst[] = [];
    public verkregenEffectieveDiensten: EffectieveDienst[] = [];
    public geselecteerdeDiensten;
    public busChauffeur: BusChauffeur;
    public onderbrekingstypes = Onderbrekingstypes;
    public dienstTypes = Diensttypes;
    public effectieveDienstenAanHetBewerken = false;
    public expandedRows = {};
    private firstDayOfWeek: Date;
    dienstAutocomplete: Dienst;
    searchResults: Dienst[];
    constructor(
        private messageService: MessageService,
        public router: Router,
        public route: ActivatedRoute,
        public fb: FormBuilder,
        private effectieveDienstenService: EffectieveDienstService,
        private dienstService: DienstService,
    ) {
        this.route.data.subscribe(data => {
            this.busChauffeur = data.busChauffeur;
        });
        try {
            this.firstDayOfWeek = this.router.getCurrentNavigation().extras.state.maandag;
            this.bewerkteEffectieveDiensten = this.router.getCurrentNavigation().extras.state.effectieveDiensten;
            if (this.router.getCurrentNavigation().extras.state.dienstenWerdenOmgezet) {
                this.verkregenEffectieveDiensten = null;
            } else {
                this.verkregenEffectieveDiensten = _.cloneDeep(this.bewerkteEffectieveDiensten);
            }
        } catch (e) {
            this.redirectTo('buschauffeur-info', this.busChauffeur);
        }
    }

    ngOnInit() {}

    wijzigingenOngedaanMaken() {
        this.bewerkteEffectieveDiensten = _.cloneDeep(this.verkregenEffectieveDiensten);
    }

    compare() {
        return _.isEqual(this.bewerkteEffectieveDiensten, this.verkregenEffectieveDiensten);
    }

    addOnderbreking(effectieveDienst: EffectieveDienst) {
        let ond: EffectieveOnderbreking = new EffectieveOnderbreking();
        ond.effectieveStart = effectieveDienst.start;
        ond.effectiefEinde = effectieveDienst.start;
        effectieveDienst.effectieveOnderbrekingen.push(ond);
        effectieveDienst.effectieveOnderbrekingen = _.clone(effectieveDienst.effectieveOnderbrekingen);
    }

    deleteOnderbreking(effectieveDienst: EffectieveDienst, ond: EffectieveOnderbreking) {
        effectieveDienst.effectieveOnderbrekingen = effectieveDienst.effectieveOnderbrekingen.filter(val => val !== ond);
        effectieveDienst.effectieveOnderbrekingen = _.clone(effectieveDienst.effectieveOnderbrekingen);
    }

    addEffectieveDienst(ed?: EffectieveDienst) {
        if (ed == null) {
            let nieuweEffectieveDienst = new EffectieveDienst();
            nieuweEffectieveDienst.busChauffeurId = this.busChauffeur.id;
            nieuweEffectieveDienst.type = 1;
            nieuweEffectieveDienst.andereMinuten = 0;
            this.bewerkteEffectieveDiensten.push(nieuweEffectieveDienst);
        } else {
            this.bewerkteEffectieveDiensten.push(ed);
        }
        this.bewerkteEffectieveDiensten = _.clone(this.bewerkteEffectieveDiensten);
    }

    deleteEffectieveDienst(ed: EffectieveDienst[]) {
        ed.forEach(dienst => {
            this.bewerkteEffectieveDiensten = this.bewerkteEffectieveDiensten.filter(val => val !== dienst);
        });
        this.geselecteerdeDiensten = [];
        this.bewerkteEffectieveDiensten = _.clone(this.bewerkteEffectieveDiensten);
    }

    redirectTo(voorvoegsel: string, bc: any) {
        this.router.navigate([`../${voorvoegsel}/${bc.id}`]);
    }

    reset() {
        this.effectieveDienstenAanHetBewerken = true;
        this.effectieveDienstenService
            .deleteEffectieveDiensten$(this.route.snapshot.params.jaar, this.route.snapshot.params.week, this.busChauffeur)
            .subscribe(
                val => {
                    if (val) {
                        this.messageService.add({severity: 'success', summary: 'De diensten werden gereset!'});
                        this.router.navigate([`../buschauffeur-info/${this.busChauffeur.id}`]);
                        this.effectieveDienstenAanHetBewerken = false;
                    }
                },
                (error: HttpErrorResponse) => {
                    if (error.error instanceof ProgressEvent) {
                        this.messageService.add({severity: 'error', summary: 'De databank is onbereikbaar!'});
                    } else {
                        console.log(error);
                        this.messageService.add({severity: 'error', summary: error.error, life: 10000});
                    }
                    this.effectieveDienstenAanHetBewerken = false;
                },
            );
    }

    effectieveDienstenAanpassen() {
        this.effectieveDienstenAanHetBewerken = true;
        let uitvoer: EffectieveDienst[] = this.bewerkteEffectieveDiensten;
        this.effectieveDienstenService
            .postEffectieveDiensten$(this.route.snapshot.params.jaar, this.route.snapshot.params.week, this.busChauffeur, uitvoer)
            .subscribe(
                val => {
                    if (val) {
                        this.messageService.add({severity: 'success', summary: 'De diensten werden opgeslaan!'});
                        this.router.navigate([`../buschauffeur-info/${this.busChauffeur.id}`], {
                            queryParams: {jaar: this.route.snapshot.params.jaar, week: this.route.snapshot.params.week},
                        });
                        this.effectieveDienstenAanHetBewerken = false;
                    }
                },
                (error: HttpErrorResponse) => {
                    console.log(error);
                    this.messageService.clear();
                    if (error.error instanceof ProgressEvent) {
                        this.messageService.add({severity: 'error', summary: 'De databank is onbereikbaar!'});
                    } else if (typeof error.error == 'string') {
                        this.messageService.add({severity: 'error', summary: error.error, life: 10000});
                    } else if (typeof error.error == 'object') {
                        for (let key in error.error.errors) {
                            let errorToShow = error.error.errors[key][0];
                            if (errorToShow != null) {
                                this.messageService.add({severity: 'error', summary: errorToShow, life: 10000});
                            } else {
                                this.messageService.add({severity: 'error', summary: 'Er liep iets fout!', life: 10000});
                            }
                        }
                    } else {
                        this.messageService.add({severity: 'error', summary: 'Er liep iets fout!', life: 10000});
                    }
                    this.effectieveDienstenAanHetBewerken = false;
                },
            );
    }

    newDate(invoer: string) {
        var uitvoer: Date = new Date(invoer);
        if (moment(uitvoer).isValid()) {
            return uitvoer;
        } else {
            return null;
        }
    }

    changeDienstType(item: EffectieveDienst, event) {
        item.type = event.id;
        item.andereMinuten = null;
        if (item.type > 1) {
            item.naam = Diensttypes[event.id].label;
            delete this.expandedRows[item.dataKey];
            item.effectieveOnderbrekingen = [];
        } else {
            item.naam = null;
            item.andereMinuten = 0;
        }
    }

    changeDag(item: EffectieveDienst, event) {
        if (item.start == null) {
            item.start = this.newDate(event);
        } else if (item.einde == null) {
            item.einde = this.newDate(event);
        }
    }

    changedDienstAutocomplete(event: Dienst) {
        this.addEffectieveDienst(event.toEffectieveDienst(this.firstDayOfWeek, this.busChauffeur));
        this.dienstAutocomplete = null;
    }

    dienstZoekenByName(event) {
        this.dienstService.tryGetDienstByName(event.query).subscribe(
            val => {
                if (val) {
                    this.searchResults = val;
                }
            },
            (error: HttpErrorResponse) => {
                this.messageService.clear();
                if (error.error instanceof ProgressEvent) {
                    this.messageService.add({severity: 'error', summary: 'De databank is onbereikbaar!'});
                } else {
                    console.log(error);
                    this.messageService.add({severity: 'error', summary: error.error, life: 10000});
                }
                this.effectieveDienstenAanHetBewerken = false;
            },
        );
    }
}
