export class EffectieveOnderbreking {
    id: string;
    effectieveStart: Date;
    effectiefEinde: Date;
    type: number;
    dataKey: number;

    constructor() {
        this.dataKey = Math.random();
    }

    static fromJSON(json: any): EffectieveOnderbreking {
        let item = new EffectieveOnderbreking();
        item.id = json.id;
        let effectieveStart = new Date(json.effectieveStart);
        // effectieveStart.setTime(effectieveStart.getTime() - effectieveStart.getTimezoneOffset() * 60 * 1000);
        effectieveStart.setSeconds(0);
        item.effectieveStart = effectieveStart;
        let effectiefEinde = new Date(json.effectiefEinde);
        // effectiefEinde.setTime(effectiefEinde.getTime() - effectiefEinde.getTimezoneOffset() * 60 * 1000);
        effectiefEinde.setSeconds(0);
        item.effectiefEinde = effectiefEinde;
        item.type = json.type;
        return item;
    }
}
