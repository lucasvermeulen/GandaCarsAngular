export let DagenVanDeWeek = [
    {id: null, label: 'Selecteer een dag'},
    {id: 1, label: 'Maandag'},
    {id: 2, label: 'Dinsdag'},
    {id: 3, label: 'Woensdag'},
    {id: 4, label: 'Donderdag'},
    {id: 5, label: 'Vrijdag'},
    {id: 6, label: 'Zaterdag'},
    {id: 7, label: 'Zondag'},
];
