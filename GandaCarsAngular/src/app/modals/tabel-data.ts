import {Dienst} from './dienst';

export class tabelData {
    jaar: number;
    diensten: Dienst[] = [];
    aantal: number = 0;
}
