import {Dienst} from './dienst';
import {EffectieveDienst} from './effectieve-dienst';

export class BusChauffeur {
    id: string;
    voornaam: string;
    achternaam: string;
    uurloon: number;
    geboorteDatum: Date;
    stelsel: number;
    diensten: Dienst[] = [];
    effectieveDiensten: EffectieveDienst[] = [];
    dataKey: number;
    straatnaam: String;
    huisnummer: String;
    postcode: String;
    stad: String;
    land: String;
    afstandKM: number;
    overUren: number;
    aantalDagenJaarlijksVerlof: number;

    constructor() {
        this.dataKey = Math.random();
    }

    static fromJSON(json: any): BusChauffeur {
        let item = new BusChauffeur();
        item.id = json.id;
        item.voornaam = json.voornaam;
        item.achternaam = json.achternaam;
        item.uurloon = json.uurloon;
        item.straatnaam = json.straatnaam;
        item.huisnummer = json.huisnummer;
        item.postcode = json.postcode;
        item.stad = json.stad;
        item.land = json.land;
        let geboorteDatum = new Date(json.geboorteDatum);
        geboorteDatum.setSeconds(0);
        item.geboorteDatum = geboorteDatum;
        item.stelsel = json.stelsel;
        item.diensten = json.diensten.map(Dienst.fromJSON);
        item.effectieveDiensten = json.effectieveDiensten.map(x => EffectieveDienst.fromJSON(x, item));
        item.afstandKM = json.afstandKM;
        item.overUren = json.overUren;
        item.aantalDagenJaarlijksVerlof = json.aantalDagenJaarlijksVerlof;
        return item;
    }
}
