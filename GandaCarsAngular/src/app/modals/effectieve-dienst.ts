import {BusChauffeur} from './bus-chauffeur';
import {EffectieveOnderbreking} from './effectieve-onderbreking';

export class EffectieveDienst {
    id: string;
    naam: string;
    start: Date;
    einde: Date;
    busChauffeur: any;
    busChauffeurId: string;
    gerelateerdeDienst: EffectieveDienst;
    dagVanToevoegen: Date;
    andereMinuten: number;
    effectieveOnderbrekingen: EffectieveOnderbreking[] = [];
    dataKey: number;
    type: number;

    constructor() {
        this.dataKey = Math.random();
    }
    static fromJSON(json: any, chauffeur?: BusChauffeur): EffectieveDienst {
        let item = new EffectieveDienst();
        item.id = json.id;
        item.naam = json.naam;
        let start = new Date(json.start);
        start.setSeconds(0);
        item.start = start;
        let einde = new Date(json.einde);
        einde.setSeconds(0);
        item.einde = einde;
        if (chauffeur) {
            item.busChauffeur = JSON.stringify(chauffeur);
        } else {
            item.busChauffeur = json.busChauffeur;
        }
        item.busChauffeurId = item.busChauffeur.id;
        item.gerelateerdeDienst = json.gerelateerdeDienst;
        item.dagVanToevoegen = json.dagVanToevoegen;
        item.andereMinuten = json.andereMinuten;
        item.effectieveOnderbrekingen = json.effectieveOnderbrekingen.map(EffectieveOnderbreking.fromJSON);
        item.type = json.type;
        return item;
    }
}
