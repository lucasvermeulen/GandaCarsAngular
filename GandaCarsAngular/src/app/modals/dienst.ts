import * as _ from 'lodash';
import {EffectieveDienst} from './effectieve-dienst';
import {EffectieveOnderbreking} from './effectieve-onderbreking';
import {Onderbreking} from './onderbreking';
export class Dienst {
    id: string;
    naam: string;
    startUur: string;
    eindUur: string;
    startDag: number;
    eindDag: number;
    busChauffeur: string;
    busChauffeurId: string;
    onderbrekingen: Onderbreking[] = [];
    dataKey: number;

    constructor() {
        this.dataKey = Math.random();
    }
    static fromJSON(json: any): Dienst {
        let item = new Dienst();
        item.id = json.id;
        item.naam = json.naam;
        item.startUur = json.startUur != null ? `${json.startUur.split(':')[0]}:${json.startUur.split(':')[1]}` : null;
        item.eindUur = json.eindUur != null ? `${json.eindUur.split(':')[0]}:${json.eindUur.split(':')[1]}` : null;
        item.startDag = json.startDag;
        item.eindDag = json.eindDag;
        item.busChauffeur = json.busChauffeur;
        item.busChauffeurId = json.busChauffeur ? json.busChauffeur.id : null;
        item.onderbrekingen = json.onderbrekingen.map(Onderbreking.fromJSON);
        return item;
    }

    toEffectieveDienst(huidigeDatum: Date, busChauffeur): EffectieveDienst {
        let ed = new EffectieveDienst();
        ed.busChauffeur = busChauffeur;
        ed.busChauffeurId = busChauffeur.id;
        ed.naam = this.naam;
        ed.andereMinuten = 0;
        ed.type = 1;
        this.onderbrekingen.forEach(dienstOnderbreking => {
            let eo: EffectieveOnderbreking = new EffectieveOnderbreking();
            let startOnderbreking: Date = _.cloneDeep(huidigeDatum);
            startOnderbreking.setDate(_.cloneDeep(huidigeDatum).getDate() + dienstOnderbreking.startDag - 1);
            startOnderbreking.setHours(parseInt(dienstOnderbreking.startUur.split(':')[0]));
            startOnderbreking.setMinutes(parseInt(dienstOnderbreking.startUur.split(':')[1]));
            let eindeOnderbreking: Date = _.cloneDeep(huidigeDatum);
            eindeOnderbreking.setDate(_.cloneDeep(huidigeDatum).getDate() + dienstOnderbreking.eindDag - 1);
            eindeOnderbreking.setHours(parseInt(dienstOnderbreking.eindUur.split(':')[0]));
            eindeOnderbreking.setMinutes(parseInt(dienstOnderbreking.eindUur.split(':')[1]));
            eo.effectieveStart = startOnderbreking;
            eo.effectiefEinde = eindeOnderbreking;
            eo.type = dienstOnderbreking.type;
            ed.effectieveOnderbrekingen.push(eo);
        });
        let startDienst: Date = _.cloneDeep(huidigeDatum);
        let eindeDienst: Date = _.cloneDeep(huidigeDatum);
        if (this.startDag == 7 && this.eindDag == 1) {
            startDienst.setDate(_.cloneDeep(huidigeDatum).getDate() + this.startDag - 1);
            eindeDienst.setDate(_.cloneDeep(huidigeDatum).getDate() + this.eindDag - 1 + 7);
        } else {
            startDienst.setDate(_.cloneDeep(huidigeDatum).getDate() + this.startDag - 1);
            eindeDienst.setDate(_.cloneDeep(huidigeDatum).getDate() + this.eindDag - 1);
        }
        startDienst.setHours(parseInt(this.startUur.split(':')[0]));
        startDienst.setMinutes(parseInt(this.startUur.split(':')[1]));
        eindeDienst.setHours(parseInt(this.eindUur.split(':')[0]));
        eindeDienst.setMinutes(parseInt(this.eindUur.split(':')[1]));
        ed.start = startDienst;
        ed.einde = eindeDienst;
        return ed;
    }
}
