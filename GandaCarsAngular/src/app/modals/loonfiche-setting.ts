import {BusChauffeur} from './bus-chauffeur';
import {Instellingen} from './instellingen';

export class LoonficheSetting {
    id: string;
    jaar: number;
    maand: number;
    busChauffeur: BusChauffeur;
    busChauffeurId: string;
    milisecondenGewaarborgdLoonEersteWeek = 0;
    milisecondenGewaarborgdLoonTweedeWeek = 0;
    milisecondenGewaarborgdLoonLaatsteDeel = 0;
    rsz = 0;
    bedrijsvoorheffing = 0;
    fietsRitten = 0;
    ARABvergoeding = 0;
    ontvangenVoorschot1 = 0;
    ontvangenVoorschot2 = 0;
    voorschotVolgendeMaand = 0;
    overurenOpnemen = 0;
    resterendeUrenDezeMaand = 0;
    loonbeslag = 0;
    gewerkteUrenDezeMaand = 0;

    constructor(jaar: number, maand: number, instellingen?: Instellingen, busChauffeur?: BusChauffeur) {
        this.jaar = jaar;
        this.maand = maand;
        this.busChauffeur = busChauffeur ? busChauffeur : null;
        this.busChauffeurId = busChauffeur ? busChauffeur.id : null;
        this.ontvangenVoorschot1 = instellingen ? instellingen.defaultVoorschot1 : 0;
        this.ontvangenVoorschot2 = instellingen ? instellingen.defaultVoorschot2 : 0;
        this.voorschotVolgendeMaand = instellingen ? instellingen.defaultVoorschotVolgendeMaand : 0;
    }

    static fromJSON(json: any): LoonficheSetting {
        if (json != null) {
            let item = new LoonficheSetting(json.jaar, json.maand, null, json.busChauffeur ? BusChauffeur.fromJSON(json.busChauffeur) : null);
            item.id = json.id;
            item.loonbeslag = json.loonbeslag;
            item.milisecondenGewaarborgdLoonEersteWeek = json.milisecondenGewaarborgdLoonEersteWeek;
            item.milisecondenGewaarborgdLoonTweedeWeek = json.milisecondenGewaarborgdLoonTweedeWeek;
            item.milisecondenGewaarborgdLoonLaatsteDeel = json.milisecondenGewaarborgdLoonLaatsteDeel;
            item.rsz = json.rsz;
            item.bedrijsvoorheffing = json.bedrijsvoorheffing;
            item.fietsRitten = json.fietsRitten;
            item.ARABvergoeding = json.araBvergoeding;
            item.ontvangenVoorschot1 = json.ontvangenVoorschot1;
            item.ontvangenVoorschot2 = json.ontvangenVoorschot2;
            item.voorschotVolgendeMaand = json.voorschotVolgendeMaand;
            item.overurenOpnemen = json.overurenOpnemen;
            item.gewerkteUrenDezeMaand = json.gewerkteUrenDezeMaand;
            return item;
        }
        return null;
    }

    isAllesCorrect() {
        if (
            this.id !== null &&
            this.milisecondenGewaarborgdLoonEersteWeek !== null &&
            this.milisecondenGewaarborgdLoonTweedeWeek !== null &&
            this.milisecondenGewaarborgdLoonLaatsteDeel !== null &&
            this.rsz !== null &&
            this.bedrijsvoorheffing !== null &&
            this.fietsRitten !== null &&
            this.ARABvergoeding !== null &&
            this.ontvangenVoorschot1 !== null &&
            this.ontvangenVoorschot2 !== null &&
            this.voorschotVolgendeMaand !== null &&
            this.gewerkteUrenDezeMaand !== null
        ) {
            return true;
        } else {
            return false;
        }
    }
}
