export class Feestdag {
    id: string;
    dag: Date;
    naam: string;
    dataKey: number;

    constructor() {
        this.dataKey = Math.random();
    }
    static fromJSON(json: any): Feestdag {
        let item = new Feestdag();
        item.id = json.id;
        item.naam = json.naam;
        let dag = new Date(json.dag);
        // dag.setTime(dag.getTime() - dag.getTimezoneOffset() * 60 * 1000);
        dag.setSeconds(0);
        item.dag = dag;
        return item;
    }
}
