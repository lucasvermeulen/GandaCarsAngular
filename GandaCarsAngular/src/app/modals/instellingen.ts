export class Instellingen {
    id: string;
    aantalMinutenAdministratieveTijdVoorDienst: number;
    aantalMinutenAdministratieveTijdVoorDienstMetMeerDan2Onderbrekingen: number;
    defaultVoorschot1: number;
    defaultVoorschot2: number;
    defaultVoorschotVolgendeMaand: number;
    gewaarborgdLoonEersteWeek: number;
    gewaarborgdLoonTweedeWeek: number;
    gewaarbordLoonLaatsteDeel: number;
    fietsVergoeding: number;
    nachtwerk: number;
    onderbrekingsVergoeding: number;
    premie: number;
    zaterdag: number;
    amplitudeMiliseconds: number;
    nachtwerkStart: number;
    nachtwerkEinde: number;
    nachtwerkNaStartAdministratieveTijd: number;
    nachtwerkVoorEindeAdministratieveTijd: number;
    werktijdFeestdag: number;
    aantalUrenStandaardWerkdag: number;
    defaultGeselecteerdeKolommenBijBuschauffeurTabel: string;
    overurenAanpassen: boolean;
    private _localBuschauffeurTabel: [];
    get localBuschauffeurTabel(): [] {
        return this._localBuschauffeurTabel;
    }
    set localBuschauffeurTabel(value: []) {
        this.defaultGeselecteerdeKolommenBijBuschauffeurTabel = JSON.stringify(value);
        this._localBuschauffeurTabel = value;
    }

    static fromJSON(json: any): Instellingen {
        const item = new Instellingen();
        item.id = json.id;
        item.aantalUrenStandaardWerkdag = json.aantalUrenStandaardWerkdag;
        item.aantalMinutenAdministratieveTijdVoorDienst = json.aantalMinutenAdministratieveTijdVoorDienst;
        item.aantalMinutenAdministratieveTijdVoorDienstMetMeerDan2Onderbrekingen =
            json.aantalMinutenAdministratieveTijdVoorDienstMetMeerDan2Onderbrekingen;
        item.defaultVoorschot1 = json.defaultVoorschot1;
        item.defaultVoorschot2 = json.defaultVoorschot2;
        item.defaultVoorschotVolgendeMaand = json.defaultVoorschotVolgendeMaand;
        item.gewaarborgdLoonEersteWeek = json.gewaarborgdLoonEersteWeek;
        item.gewaarborgdLoonTweedeWeek = json.gewaarborgdLoonTweedeWeek;
        item.gewaarbordLoonLaatsteDeel = json.gewaarbordLoonLaatsteDeel;
        item.fietsVergoeding = json.fietsVergoeding;
        item.nachtwerk = json.nachtwerk;
        item.onderbrekingsVergoeding = json.onderbrekingsVergoeding;
        item.premie = json.premie;
        item.zaterdag = json.zaterdag;
        item.nachtwerkStart = json.nachtwerkStart;
        item.nachtwerkEinde = json.nachtwerkEinde;
        item.nachtwerkNaStartAdministratieveTijd = json.nachtwerkNaStartAdministratieveTijd;
        item.nachtwerkVoorEindeAdministratieveTijd = json.nachtwerkVoorEindeAdministratieveTijd;
        item.amplitudeMiliseconds = json.amplitudeMiliseconds;
        item.werktijdFeestdag = json.werktijdFeestdag;
        item.defaultGeselecteerdeKolommenBijBuschauffeurTabel = json.defaultGeselecteerdeKolommenBijBuschauffeurTabel;
        item.localBuschauffeurTabel = JSON.parse(json.defaultGeselecteerdeKolommenBijBuschauffeurTabel);
        item.overurenAanpassen = json.overurenAanpassen;
        return item;
    }
}
