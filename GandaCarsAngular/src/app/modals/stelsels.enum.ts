export let Stelsels = [
    {id: null, label: 'Selecteer een stelsel'},
    {id: 1, label: 'Bus'},
    {id: 2, label: 'Bijzondere dienst'},
    {id: 3, label: 'Garage'},
    {id: 4, label: 'Bus + bijzondere dienst'},
    {id: 5, label: 'Bus + garage'},
];
