export class Onderbreking {
    id: string;
    startUur: string;
    eindUur: string;
    startDag: number;
    eindDag: number;
    type: number;
    dataKey: number;

    constructor() {
        this.dataKey = Math.random();
    }
    static fromJSON(json: any): Onderbreking {
        let item = new Onderbreking();
        item.id = json.id;
        //let startUur = new Date(json.startUur);
        // startUur.setTime(startUur.getTime() - startUur.getTimezoneOffset() * 60 * 1000);
        // startUur.setSeconds(0);
        item.startUur = `${json.startUur.split(':')[0]}:${json.startUur.split(':')[1]}`;
        //let eindUur = new Date(json.eindUur);
        // eindUur.setTime(eindUur.getTime() - eindUur.getTimezoneOffset() * 60 * 1000);
        // eindUur.setSeconds(0);
        item.eindUur = `${json.eindUur.split(':')[0]}:${json.eindUur.split(':')[1]}`;
        item.startDag = json.startDag;
        item.eindDag = json.eindDag;
        item.type = json.type;
        return item;
    }
}
