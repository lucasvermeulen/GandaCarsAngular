import * as moment from 'moment';
import {BusChauffeur} from './bus-chauffeur';
import {DagExport} from './dag-export';
import {EffectieveDienst} from './effectieve-dienst';
import {Instellingen} from './instellingen';
import {LoonficheSetting} from './loonfiche-setting';

export class LoonficheTabel {
    totaalDagExports: DagExport;
    bewerkteLoonficheSetting: LoonficheSetting;
    verkregenLoonficheSetting: LoonficheSetting;
    dagExports: DagExport[];
    busChauffeur: BusChauffeur;
    effectieveDiensten: EffectieveDienst[];
    maand: moment.Moment;
    instellingen: Instellingen;
    public gepresteerdeArbeid: number;
    public nachtwerk: number;
    public zaterdag: number;
    public overwerk: number;
    public zondag: number;
    public aantalOnderbrekingen: number;
    public stationnement100: number;
    public stationnement50: number;
    public maandelijksePremie: number;
    public resterendeUrenDezeMaand: number;
    public uitbetalingOveruren: number;
    public subtotaal: number;
    public algemeenTotaal: number;
    public brutoloon: number;
    public gewaarbordLoonEersteWeek: number;
    public gewaarbordLoonTweedeWeek: number;
    public gewaarbordLoonLaatsteDeel: number;
    public belastbaarBedrag: number;
    public fietsvergoeding: number;
    public nettoLoon: number;
    public subTotaalVoorschotten: number;
    public totaalTeStoren: number;
    public opgespaardeOveruren: number;
    public saldoOverurenDezeMaand: number;
    public jvNogOpTeNemen: number;
    public tePresterenUren: number;
    updateValues() {
        this.gepresteerdeArbeid = this.urenMaalTijd(this.totaalDagExports.totaalAT, this.busChauffeur.uurloon);
        this.nachtwerk = this.urenMaalTijd(this.totaalDagExports.nacht, this.instellingen.nachtwerk);
        this.zaterdag = this.urenMaalTijd(this.totaalDagExports.zat, (this.instellingen.zaterdag / 100) * this.busChauffeur.uurloon);
        this.overwerk = this.urenMaalTijd(this.totaalDagExports.overwerk, this.busChauffeur.uurloon);
        this.zondag = this.urenMaalTijd(this.totaalDagExports.zon, this.busChauffeur.uurloon);
        this.aantalOnderbrekingen = this.effectieveDiensten.filter(
            element => element.effectieveOnderbrekingen.filter(y => y.type == 1).length > 0,
        ).length;
        this.stationnement100 = this.urenMaalTijd(this.totaalDagExports.statAT + this.totaalDagExports.statRest, this.busChauffeur.uurloon);
        this.stationnement50 = this.urenMaalTijd(this.totaalDagExports.stat50, this.busChauffeur.uurloon / 2);
        this.maandelijksePremie =
            new Set(this.effectieveDiensten.filter(x => x.type == 1).map(y => y.start.toDateString())).size >= 10 ? this.instellingen.premie : 0;
        this.tePresterenUren = 0;
        const monthIndex = this.maand.month();
        const date = new Date(this.maand.year(), monthIndex, 1);
        while (date.getMonth() === monthIndex) {
            if (date.getDay() != 0) {
                this.tePresterenUren += this.instellingen.aantalUrenStandaardWerkdag;
            }
            date.setDate(date.getDate() + 1);
        }
        this.tePresterenUren -=
            this.effectieveDiensten.filter(x => x.type === 2 || x.type === 3).length * this.instellingen.aantalUrenStandaardWerkdag;
        this.resterendeUrenDezeMaand = this.totaalDagExports.totaalAT - this.tePresterenUren;
        this.uitbetalingOveruren = this.urenMaalTijd(this.bewerkteLoonficheSetting.overurenOpnemen, this.busChauffeur.uurloon);
        this.subtotaal =
            this.gepresteerdeArbeid +
            this.nachtwerk +
            this.zaterdag +
            this.uitbetalingOveruren +
            this.zondag +
            this.instellingen.onderbrekingsVergoeding * this.aantalOnderbrekingen +
            this.stationnement100 +
            this.stationnement50 +
            this.overwerk;
        this.algemeenTotaal = this.subtotaal + this.maandelijksePremie;
        this.gewaarbordLoonEersteWeek = this.urenMaalTijd(
            this.bewerkteLoonficheSetting.milisecondenGewaarborgdLoonEersteWeek,
            (this.busChauffeur.uurloon * this.instellingen.gewaarborgdLoonEersteWeek) / 100,
        );
        this.gewaarbordLoonTweedeWeek = this.urenMaalTijd(
            this.bewerkteLoonficheSetting.milisecondenGewaarborgdLoonTweedeWeek,
            (this.busChauffeur.uurloon * this.instellingen.gewaarborgdLoonTweedeWeek) / 100,
        );
        this.gewaarbordLoonLaatsteDeel = this.urenMaalTijd(
            this.bewerkteLoonficheSetting.milisecondenGewaarborgdLoonLaatsteDeel,
            (this.busChauffeur.uurloon * this.instellingen.gewaarbordLoonLaatsteDeel) / 100,
        );
        this.brutoloon = this.algemeenTotaal + this.gewaarbordLoonEersteWeek + this.gewaarbordLoonTweedeWeek + this.gewaarbordLoonLaatsteDeel;
        this.belastbaarBedrag = this.brutoloon - this.bewerkteLoonficheSetting.rsz;
        this.fietsvergoeding = this.instellingen.fietsVergoeding * this.busChauffeur.afstandKM * this.bewerkteLoonficheSetting.fietsRitten;
        this.nettoLoon =
            this.belastbaarBedrag -
            this.bewerkteLoonficheSetting.bedrijsvoorheffing +
            this.fietsvergoeding +
            this.bewerkteLoonficheSetting.ARABvergoeding;
        this.subTotaalVoorschotten =
            this.nettoLoon - (this.bewerkteLoonficheSetting.ontvangenVoorschot1 + this.bewerkteLoonficheSetting.ontvangenVoorschot2);

        this.totaalTeStoren =
            this.subTotaalVoorschotten + this.bewerkteLoonficheSetting.voorschotVolgendeMaand - this.bewerkteLoonficheSetting.loonbeslag;
        if (this.verkregenLoonficheSetting == null) {
            this.opgespaardeOveruren = this.busChauffeur.overUren;
        } else {
            this.opgespaardeOveruren = this.busChauffeur.overUren + this.verkregenLoonficheSetting.overurenOpnemen - this.resterendeUrenDezeMaand;
        }
        this.saldoOverurenDezeMaand = this.opgespaardeOveruren - this.bewerkteLoonficheSetting.overurenOpnemen + this.resterendeUrenDezeMaand;

        this.jvNogOpTeNemen =
            this.busChauffeur.aantalDagenJaarlijksVerlof -
            this.busChauffeur.effectieveDiensten.filter(
                x =>
                    x.type == 5 &&
                    x.start.getFullYear() == this.bewerkteLoonficheSetting.jaar &&
                    x.start.getMonth() + 1 <= this.bewerkteLoonficheSetting.maand,
            ).length;
    }

    private urenMaalTijd(tijdMiliseconds: number, bedragPerUur: number) {
        if (tijdMiliseconds == null) {
            return 0;
        }
        const spilttedTijd = this.msToTime(tijdMiliseconds).split(':');
        return parseInt(spilttedTijd[0]) * bedragPerUur + (parseInt(spilttedTijd[1]) / 60) * bedragPerUur;
    }

    private msToTime(s: number = 0): string {
        const ms = s % 1000;
        s = (s - ms) / 1000;
        const secs = s % 60;
        s = (s - secs) / 60;
        const mins = s % 60;
        const hrs = (s - mins) / 60;
        let uitvoer = '';
        if (s >= 0) {
            if (hrs.toString().length < 2) {
                uitvoer = `0${hrs}:`;
            } else {
                uitvoer = `${hrs}:`;
            }
            if (mins.toString().length < 2) {
                uitvoer += `0${mins}`;
            } else {
                uitvoer += `${mins}`;
            }
        } else {
            if (hrs.toString().length < 3) {
                uitvoer = `-0${Math.abs(hrs)}:`;
            } else {
                uitvoer = `${hrs}:`;
            }
            if (Math.abs(mins).toString().length < 2) {
                uitvoer += `0${Math.abs(mins)}`;
            } else {
                uitvoer += `${Math.abs(mins)}`;
            }
        }
        return uitvoer;
    }
}
