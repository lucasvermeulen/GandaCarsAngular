export let Diensttypes = [
    {id: null, label: 'Selecteer een type'},
    {id: 1, label: 'Dienst'},
    {id: 2, label: 'Ziekte'},
    {id: 3, label: 'Ouderschapsverlof'},
    {id: 4, label: 'Feestdag'},
    {id: 5, label: 'Jaarlijksverlof'},
];
