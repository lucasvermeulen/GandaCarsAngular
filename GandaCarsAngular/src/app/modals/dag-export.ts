import * as _ from 'lodash';
import * as moment from 'moment';
import {BehaviorSubject} from 'rxjs';
import {EffectieveDienst} from './effectieve-dienst';
import {Feestdag} from './feestdag';
import {Instellingen} from './instellingen';

export class DagExport {
    effectieveDiensten: BehaviorSubject<EffectieveDienst[]> = new BehaviorSubject([]);
    feestdagen: BehaviorSubject<Feestdag[]> = new BehaviorSubject([]);
    jaar: number;
    datum: Date;
    maand: string;
    dagNummer: number;
    dagNaam: string;
    dienstNamen: string[] = [];
    rijtijd = 0;
    statAT = 0;
    statRest = 0;
    stat50 = 0;
    admin = 0;
    ander = 0;
    ampl = 0;
    apdp = 0;
    totaalAT = 0;
    nacht = 0;
    zat = 0;
    zon = 0;
    ond1 = 0;
    ond2 = 0;
    ond3 = 0;
    onvPres = 0;
    overwerk = 0;
    instellingen: Instellingen;

    constructor(datum: Date, maand: string, naam: string, instellingen: Instellingen) {
        this.jaar = datum.getFullYear();
        this.maand = maand;
        this.dagNummer = datum.getDate();
        this.datum = _.clone(datum);
        this.dagNaam = naam;
        this.instellingen = instellingen;
        this.effectieveDiensten.subscribe(effectieveDiensten => {
            this.changeValues();
        });
        this.feestdagen.subscribe(feestdagen => {
            this.changeValues();
        });
    }

    changeValues() {
        this.allesOpDefaultWaarden();
        _.orderBy(this.effectieveDiensten.value, ['type'], ['asc']).forEach((ed: EffectieveDienst) => {
            if (ed.type == 1) {
                let start = moment(ed.start);
                let einde = moment(ed.gerelateerdeDienst !== null ? new Date(ed.gerelateerdeDienst.einde) : ed.einde);
                let totaleTijdOnderbrekingen = ed.effectieveOnderbrekingen
                    .filter(x => x.type == 1)
                    .reduce((a, b) => a + moment(b.effectiefEinde).diff(moment(b.effectieveStart)), 0);
                let totaleTijdStationnementen = ed.effectieveOnderbrekingen
                    .filter(x => x.type == 2)
                    .reduce((a, b) => a + moment(b.effectiefEinde).diff(moment(b.effectieveStart)), 0);
                this.dienstNamen.push(ed.naam);
                this.rijtijd += einde.diff(start) - totaleTijdOnderbrekingen - totaleTijdStationnementen;
                this.statAT = ed.effectieveOnderbrekingen
                    .filter(x => x.type == 2)
                    .reduce(
                        (a, b) =>
                            a +
                            (moment(b.effectiefEinde).diff(moment(b.effectieveStart)) > 900000
                                ? 900000
                                : moment(b.effectiefEinde).diff(moment(b.effectieveStart)) || 0),
                        0,
                    );
                this.statRest = ed.effectieveOnderbrekingen
                    .filter(x => x.type == 2)
                    .reduce(
                        (a, b) =>
                            a +
                            (moment(b.effectiefEinde).diff(moment(b.effectieveStart)) - 900000 > 1800000
                                ? 1800000
                                : Math.max(0, moment(b.effectiefEinde).diff(moment(b.effectieveStart)) - 900000) || 0),
                        0,
                    );
                this.stat50 = ed.effectieveOnderbrekingen
                    .filter(x => x.type == 2)
                    .reduce((a, b) => a + Math.max(0, moment(b.effectiefEinde).diff(moment(b.effectieveStart)) - 2700000), 0);
                this.totaalAT =
                    this.rijtijd +
                    this.statAT +
                    this.statRest +
                    (ed.effectieveOnderbrekingen.filter(x => x.type == 1).length + 1) *
                        (this.instellingen.aantalMinutenAdministratieveTijdVoorDienst / 60000) *
                        60000 +
                    ed.andereMinuten * 60000;
                this.nacht = this.berekenNacht(ed);
                this.zat = ed.start.getDay() == 6 ? this.totaalAT : 0;
                this.overwerk = Math.max(0, this.totaalAT - 600);
                if (ed.type == 1) {
                    if (ed.effectieveOnderbrekingen.filter(x => x.type == 1).length < 2) {
                        this.admin +=
                            (this.instellingen.aantalMinutenAdministratieveTijdVoorDienst / 60000) *
                            (ed.effectieveOnderbrekingen.filter(x => x.type == 1).length + 1);
                    } else if (ed.effectieveOnderbrekingen.filter(x => x.type == 1).length >= 2) {
                        this.admin += this.instellingen.aantalMinutenAdministratieveTijdVoorDienstMetMeerDan2Onderbrekingen / 60000;
                    }
                }
                this.ander = ed.andereMinuten;
                this.ampl = Math.max(0, einde.diff(start) - this.instellingen.amplitudeMiliseconds);
                this.apdp = Math.max(0, 240 - this.totaalAT);
                this.zon += ed.start.getDay() == 0 ? this.totaalAT : 0;
                this.ond1 = ed.effectieveOnderbrekingen.filter(x => x.type == 1).length >= 1 ? 1 : 0;
                this.ond2 = ed.effectieveOnderbrekingen.filter(x => x.type == 1).length >= 2 ? 1 : 0;
                this.ond3 = ed.effectieveOnderbrekingen.filter(x => x.type == 1).length >= 3 ? 1 : 0;
                this.onvPres = 0;
                this.overwerk = 0;
                // effectieve dienst valt op een feestdag
                if (this.feestdagen.value.length > 0) {
                    this.dienstNamen.push('FD');
                    if (this.totaalAT != 0) {
                        this.zon = this.totaalAT;
                    } else {
                        this.rijtijd = this.instellingen.werktijdFeestdag;
                        this.totaalAT = this.instellingen.werktijdFeestdag;
                    }
                }
            } else if (ed.type == 2) {
                this.dienstNamen.push('Z');
            } else if (ed.type == 3) {
                this.dienstNamen.push('OV');
            } else if (ed.type == 4) {
                this.dienstNamen.push('FD');
                if (this.totaalAT != 0) {
                    this.zon = this.totaalAT;
                } else {
                    this.rijtijd = this.instellingen.werktijdFeestdag;
                    this.totaalAT = this.instellingen.werktijdFeestdag;
                }
            } else if (ed.type == 5) {
                this.dienstNamen.push('JV');
            }
        });
        this.feestdagen.value.forEach(fd => {
            if (this.effectieveDiensten.value.filter(dienst => dienst.start.toDateString() == fd.dag.toDateString()).length < 1) {
                this.dienstNamen.push('FD');
                this.rijtijd = this.instellingen.werktijdFeestdag;
                this.totaalAT = this.instellingen.werktijdFeestdag;
            }
        });
    }

    allesOpDefaultWaarden() {
        this.dienstNamen = [];
        this.rijtijd = 0;
        this.statAT = 0;
        this.statRest = 0;
        this.stat50 = 0;
        this.admin = 0;
        this.ander = 0;
        this.ampl = 0;
        this.apdp = 0;
        this.totaalAT = 0;
        this.nacht = 0;
        this.zat = 0;
        this.zon = 0;
        this.ond1 = 0;
        this.ond2 = 0;
        this.ond3 = 0;
        this.onvPres = 0;
        this.overwerk = 0;
    }

    berekenNacht(dienst: EffectieveDienst): number {
        var nachtwerk;
        let start = moment(dienst.start);
        let einde = moment(dienst.gerelateerdeDienst !== null ? new Date(dienst.gerelateerdeDienst.einde) : dienst.einde);
        if (start.toDate().toDateString() === einde.toDate().toDateString()) {
            let nachtwerkEind = moment(start)
                .hours(parseInt(this.msToTime(this.instellingen.nachtwerkEinde).split(':')[0]))
                .minutes(parseInt(this.msToTime(this.instellingen.nachtwerkEinde).split(':')[1]));
            let nachtwerkStart = moment(einde)
                .hours(parseInt(this.msToTime(this.instellingen.nachtwerkStart).split(':')[0]))
                .minutes(parseInt(this.msToTime(this.instellingen.nachtwerkStart).split(':')[1]));
            if (start < nachtwerkEind && start < nachtwerkStart && einde <= nachtwerkEind && einde < nachtwerkStart) {
                //'sit 1'
                nachtwerk = einde.diff(start);
            } else if (start < nachtwerkEind && start < nachtwerkStart && nachtwerkEind < einde && einde < nachtwerkStart) {
                //'sit 2'
                nachtwerk = nachtwerkEind.diff(start);
            } else if (nachtwerkEind <= start && start < nachtwerkStart && nachtwerkEind < einde && einde <= nachtwerkStart) {
                //'sit 3'
                nachtwerk = 0;
            } else if (nachtwerkEind < start && start < nachtwerkStart && nachtwerkStart < einde && nachtwerkEind < einde) {
                //'sit 4'
                nachtwerk = einde.diff(nachtwerkStart);
            } else if (nachtwerkEind < start && nachtwerkStart <= start && nachtwerkEind < einde && nachtwerkStart < einde) {
                //'sit 5'
                nachtwerk = einde.diff(start);
            } else if (start < nachtwerkEind && start < nachtwerkStart && nachtwerkStart < einde && nachtwerkEind < einde) {
                //'sit 6'
                nachtwerk = nachtwerkStart.diff(nachtwerkEind);
            }
        } else {
            let nachtwerkEindDag1 = moment(start)
                .hours(parseInt(this.msToTime(this.instellingen.nachtwerkEinde).split(':')[0]))
                .minutes(parseInt(this.msToTime(this.instellingen.nachtwerkEinde).split(':')[1]));
            let nachtwerkStartDag1 = moment(start)
                .hours(parseInt(this.msToTime(this.instellingen.nachtwerkStart).split(':')[0]))
                .minutes(parseInt(this.msToTime(this.instellingen.nachtwerkStart).split(':')[1]));
            let nachtwerkEindDag2 = moment(einde)
                .hours(parseInt(this.msToTime(this.instellingen.nachtwerkEinde).split(':')[0]))
                .minutes(parseInt(this.msToTime(this.instellingen.nachtwerkEinde).split(':')[1]));
            let nachtwerkStartDag2 = moment(einde)
                .hours(parseInt(this.msToTime(this.instellingen.nachtwerkStart).split(':')[0]))
                .minutes(parseInt(this.msToTime(this.instellingen.nachtwerkStart).split(':')[1]));
            if (start < nachtwerkEindDag1) {
                //'sit 7: begin in dag 1 nachtwerk, einde tijdens nacht dag 2'
                nachtwerk = nachtwerkEindDag1.diff(start) + einde.diff(nachtwerkStartDag1);
            } else if (nachtwerkEindDag1 <= start && start < nachtwerkStartDag1) {
                if (einde <= nachtwerkEindDag2) {
                    //'sit 8: begin overdag dag 1, einde in nacht dag 2'
                    nachtwerk = einde.diff(nachtwerkStartDag1);
                } else if (nachtwerkEindDag2 < einde) {
                    //'sit 9: begin overdag dag 1, einde overdag op dag 2'
                    nachtwerk = nachtwerkEindDag2.diff(nachtwerkStartDag1);
                }
            } else if (nachtwerkStartDag1 <= start) {
                if (einde <= nachtwerkEindDag2) {
                    //'sit 10: begin nacht, savonds dag 1, einde in nacht sochtends dag 2'
                    nachtwerk = einde.diff(start);
                } else if (nachtwerkEindDag2 < einde && einde < nachtwerkStartDag2) {
                    //'sit 11: begin begin nacht, savonds dag 1, einde overdag op dag 2'
                    nachtwerk = nachtwerkEindDag2.diff(start);
                } else if (nachtwerkStartDag2 <= einde) {
                    nachtwerk = nachtwerkEindDag2.diff(start) + einde.diff(nachtwerkStartDag2);
                    //'sit 12: begin begin nacht, savonds dag 1, einde nacht savons dag 2'
                }
            }
        }
        return nachtwerk;
    }

    msToTime(s: number = 0): string {
        const ms = s % 1000;
        s = (s - ms) / 1000;
        const secs = s % 60;
        s = (s - secs) / 60;
        const mins = s % 60;
        const hrs = (s - mins) / 60;
        let uitvoer = '';
        if (s >= 0) {
            if (hrs.toString().length < 2) {
                uitvoer = `0${hrs}:`;
            } else {
                uitvoer = `${hrs}:`;
            }
            if (mins.toString().length < 2) {
                uitvoer += `0${mins}`;
            } else {
                uitvoer += `${mins}`;
            }
        } else {
            if (hrs.toString().length < 3) {
                uitvoer = `-0${Math.abs(hrs)}:`;
            } else {
                uitvoer = `${hrs}:`;
            }
            if (Math.abs(mins).toString().length < 2) {
                uitvoer += `0${Math.abs(mins)}`;
            } else {
                uitvoer += `${Math.abs(mins)}`;
            }
        }
        return uitvoer;
    }
}
