import {Component, Input, OnInit} from '@angular/core';
import {LoonficheTabel} from '../modals/loonfiche-tabel';

@Component({
    selector: 'app-loonfiche-bewerken',
    templateUrl: './loonfiche-bewerken.component.html',
    styleUrls: ['./loonfiche-bewerken.component.scss'],
})
export class LoonficheBewerkenComponent implements OnInit {
    @Input()
    loonficheTabel: LoonficheTabel;
    @Input()
    public loading: boolean;
    constructor() {}

    ngOnInit(): void {}

    msToTime(s: number = 0): string {
        const ms = s % 1000;
        s = (s - ms) / 1000;
        const secs = s % 60;
        s = (s - secs) / 60;
        const mins = s % 60;
        const hrs = (s - mins) / 60;
        let uitvoer = '';
        if (s >= 0) {
            if (hrs.toString().length < 2) {
                uitvoer = `0${hrs}:`;
            } else {
                uitvoer = `${hrs}:`;
            }
            if (mins.toString().length < 2) {
                uitvoer += `0${mins}`;
            } else {
                uitvoer += `${mins}`;
            }
        } else {
            if (hrs.toString().length < 3) {
                uitvoer = `-0${Math.abs(hrs)}:`;
            } else {
                uitvoer = `${hrs}:`;
            }
            if (Math.abs(mins).toString().length < 2) {
                uitvoer += `0${Math.abs(mins)}`;
            } else {
                uitvoer += `${Math.abs(mins)}`;
            }
        }
        return uitvoer;
    }

    timeToMs(time: string, elementId?: string): number {
        var uitvoer;
        try {
            if (time == '' || time.match('[-]?[0-9]*:[0-5][0-9]$') == null) {
                uitvoer = 0;
            } else {
                const splittedTime = time.toString().split(':');
                if (time.substring(0, 1) != '-') {
                    uitvoer = 3600000 * parseInt(splittedTime[0]) + 60000 * parseInt(splittedTime[1]);
                } else {
                    uitvoer = 3600000 * parseInt(splittedTime[0]) - 60000 * parseInt(splittedTime[1]);
                }
            }
        } catch (e) {
            uitvoer = 0;
        }
        try {
            if (elementId != null) {
                (document.getElementById(elementId) as HTMLInputElement).value = this.msToTime(uitvoer);
            }
        } catch (e) {}
        return uitvoer;
    }
}
