import {Component, OnInit} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import * as _ from 'lodash';
import {MessageService} from 'primeng/api';
import {Feestdag} from '../modals/feestdag';
import {FeestdagenService} from '../services/feestdagen.service';

@Component({
    selector: 'app-feestdagen-aanpassen',
    templateUrl: './feestdagen-aanpassen.component.html',
    styleUrls: ['./feestdagen-aanpassen.component.scss'],
})
export class FeestdagenAanpassenComponent implements OnInit {
    public bewerkteFeestdagen: Feestdag[];
    public verkregenFeestdagen: Feestdag[];
    public loadingFeestdagen = false;
    public geselecteerdeFeestdagen: Feestdag[];
    public locale = {
        firstDayOfWeek: 1,
        dayNames: ['Zondag', 'Maandag', 'Dinsdag', 'Woensdag', 'Donderdag', 'Vrijdag', 'Zaterdag'],
        dayNamesShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
        dayNamesMin: ['Zo', 'Ma', 'Di', 'Woe', 'Do', 'Vr', 'Za'],
        monthNames: ['Januari', 'Februari', 'Maart', 'April', 'Mei', 'Juni', 'Juli', 'Augustus', 'September', 'Oktober', 'November', 'December'],
        monthNamesShort: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dec'],
        today: 'Vandaag',
        clear: 'Clear',
        dateFormat: 'dd/mm/yy',
        weekHeader: 'Wk',
    };

    constructor(
        private messageService: MessageService,
        public route: ActivatedRoute,
        public router: Router,
        private fb: FormBuilder,
        private feestdagenService: FeestdagenService,
    ) {}

    ngOnInit() {
        this.getAllFeestdagen();
    }

    compare() {
        return _.isEqual(this.bewerkteFeestdagen, this.verkregenFeestdagen);
    }

    wijzigingenOngedaanMaken() {
        this.bewerkteFeestdagen = _.cloneDeep(this.verkregenFeestdagen);
    }

    getAllFeestdagen() {
        this.loadingFeestdagen = true;
        this.feestdagenService.getAllFeestdagen$().subscribe(
            val => {
                if (val) {
                    this.bewerkteFeestdagen = val;
                    this.verkregenFeestdagen = _.cloneDeep(this.bewerkteFeestdagen);
                    this.loadingFeestdagen = false;
                }
            },
            error => {
                this.messageService.clear();
                if (error.error instanceof ProgressEvent) {
                    this.messageService.add({severity: 'error', summary: 'De databank is onbereikbaar!'});
                } else {
                    console.log(error);
                    this.messageService.add({severity: 'error', summary: error.error, life: 10000});
                }
                this.loadingFeestdagen = false;
            },
        );
    }

    addFeestdag() {
        this.bewerkteFeestdagen.push(new Feestdag());
        this.bewerkteFeestdagen = _.clone(this.bewerkteFeestdagen);
    }

    deleteFeestdag(feestdagen: Feestdag[]) {
        feestdagen.forEach(fd => {
            this.bewerkteFeestdagen = this.bewerkteFeestdagen.filter(val => val !== fd);
        });
        this.geselecteerdeFeestdagen = [];
        this.bewerkteFeestdagen = _.clone(this.bewerkteFeestdagen);
    }

    feestdagenAanpassen() {
        this.feestdagenService.postAllFeestdagen$(this.bewerkteFeestdagen).subscribe(
            val => {
                if (val) {
                    this.messageService.add({severity: 'success', summary: 'Feestdag(en) werd(en) aangepast!'});
                    this.getAllFeestdagen();
                }
            },
            error => {
                console.log(error);
                this.messageService.clear();
                if (error.error instanceof ProgressEvent) {
                    this.messageService.add({severity: 'error', summary: 'De databank is onbereikbaar!'});
                } else if (typeof error.error == 'string') {
                    this.messageService.add({severity: 'error', summary: error.error, life: 10000});
                } else if (typeof error.error == 'object') {
                    for (let key in error.error.errors) {
                        let errorToShow = error.error.errors[key][0];
                        if (errorToShow != null) {
                            this.messageService.add({severity: 'error', summary: errorToShow, life: 10000});
                        } else {
                            this.messageService.add({severity: 'error', summary: 'Er liep iets fout!', life: 10000});
                        }
                    }
                } else {
                    this.messageService.add({severity: 'error', summary: 'Er liep iets fout!', life: 10000});
                }
            },
        );
    }
}
