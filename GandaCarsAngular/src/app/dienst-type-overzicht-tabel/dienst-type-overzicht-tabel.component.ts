import {Component, OnInit} from '@angular/core';
import {DynamicDialogConfig} from 'primeng/dynamicdialog';
@Component({
    selector: 'app-dienst-type-overzicht-tabel',
    templateUrl: './dienst-type-overzicht-tabel.component.html',
    styleUrls: ['./dienst-type-overzicht-tabel.component.scss'],
})
export class DienstTypeOverzichtTabelComponent implements OnInit {
    constructor(public config: DynamicDialogConfig) {}
    public tabel = [];
    public vandaag: Date = new Date();

    ngOnInit(): void {
        this.tabel = this.config.data;
        this.tabel.sort((a, b) => b.jaar - a.jaar);
        this.tabel.forEach(jaar => {
            jaar.diensten.sort((a, b) => a.start - b.start);
        });
    }

    getTitle(item): string {
        return `${item.jaar} (${item.aantal})`;
    }
}
