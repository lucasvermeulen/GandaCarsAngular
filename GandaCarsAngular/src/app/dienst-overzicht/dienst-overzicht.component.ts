import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import * as _ from 'lodash';
import {MessageService} from 'primeng/api';
import {BusChauffeur} from '../modals/bus-chauffeur';
import {DagenVanDeWeek} from '../modals/dagen-van-de-week.enum';
import {Dienst} from '../modals/dienst';
import {Diensttypes} from '../modals/diensttypes.enum';
import {Onderbreking} from '../modals/onderbreking';
import {Onderbrekingstypes} from '../modals/onderbrekingstypes.enum';
import {BusChauffeurService} from '../services/bus-chauffeur.service';
import {DienstService} from '../services/dienst.service';
@Component({
    selector: 'app-dienst-overzicht',
    templateUrl: './dienst-overzicht.component.html',
    styleUrls: ['./dienst-overzicht.component.scss'],
})
export class DienstOverzichtComponent implements OnInit {
    public loadingDiensten = true;
    public bewerkteDiensten: Dienst[];
    public verkregenDiensten: Dienst[];
    public geselecteerdeDiensten;
    public onderbrekingstypes = Onderbrekingstypes;
    public dienstTypes = Diensttypes;
    public dagenVanDeWeek = DagenVanDeWeek;
    public busChauffeurs: BusChauffeur[];
    public searchResults: BusChauffeur[];
    public expandedRows = {};
    constructor(
        private busChauffeurService: BusChauffeurService,
        private messageService: MessageService,
        private dienstService: DienstService,
        private router: Router,
    ) {}

    ngOnInit() {
        this.getAllDiensten();
        this.getAllBuschauffeurs();
    }

    wijzigingenOngedaanMaken() {
        this.bewerkteDiensten = _.cloneDeep(this.verkregenDiensten);
    }

    dienstenOpslaan() {
        this.loadingDiensten = true;
        let dienstenToDelete = [];
        let dienstenToPut: Dienst[] = [];
        this.verkregenDiensten.forEach(oudeDienst => {
            if (this.bewerkteDiensten.find(x => _.isEqual(x, oudeDienst)) == undefined) {
                let bewerkteDienst = this.bewerkteDiensten.find(x => x.id === oudeDienst.id);
                if (bewerkteDienst != undefined) {
                    dienstenToPut.push(bewerkteDienst);
                } else {
                    dienstenToDelete.push(oudeDienst);
                }
            }
        });
        let dienstenToAdd = this.bewerkteDiensten.filter(x => x.id == null);
        this.dienstService.updateCrud(dienstenToAdd, dienstenToPut, dienstenToDelete).subscribe(
            val => {
                if (val) {
                    this.getAllDiensten();
                    this.geselecteerdeDiensten = [];
                    this.messageService.add({severity: 'success', summary: 'De diensten werden opgeslaan!'});
                }
            },
            error => {
                console.log(error);
                this.messageService.clear();
                if (error.error instanceof ProgressEvent) {
                    this.messageService.add({severity: 'error', summary: 'De databank is onbereikbaar!'});
                } else if (typeof error.error == 'string') {
                    this.messageService.add({severity: 'error', summary: error.error, life: 10000});
                } else if (typeof error.error == 'object') {
                    for (let key in error.error.errors) {
                        let errorToShow = error.error.errors[key][0];
                        if (errorToShow != null) {
                            this.messageService.add({severity: 'error', summary: errorToShow, life: 10000});
                        } else {
                            this.messageService.add({severity: 'error', summary: 'Er liep iets fout!', life: 10000});
                        }
                    }
                } else {
                    this.messageService.add({severity: 'error', summary: 'Er liep iets fout!', life: 10000});
                }
                this.loadingDiensten = false;
            },
        );
    }

    compare() {
        return _.isEqual(this.bewerkteDiensten, this.verkregenDiensten);
    }

    getAllBuschauffeurs() {
        this.busChauffeurService.getAllBusChauffeurs$().subscribe(
            val => {
                if (val) {
                    this.busChauffeurs = val;
                }
            },
            error => {
                this.messageService.clear();
                if (error.error instanceof ProgressEvent) {
                    this.messageService.add({severity: 'error', summary: 'De databank is onbereikbaar!'});
                } else {
                    console.log(error);
                    this.messageService.add({severity: 'error', summary: error.error, life: 10000});
                }
            },
        );
    }

    addOnderbreking(dienst: Dienst) {
        let ond: Onderbreking = new Onderbreking();
        ond.startDag = dienst.startDag;
        ond.eindDag = dienst.eindDag;
        dienst.onderbrekingen.push(ond);
    }

    deleteOnderbreking(dienst: Dienst, ond: Onderbreking) {
        dienst.onderbrekingen = dienst.onderbrekingen.filter(val => val !== ond);
    }

    searchBuschauffeurs(event) {
        this.searchResults = this.busChauffeurs.filter(
            x =>
                x.achternaam.toLowerCase().includes((event.query as String).toLowerCase()) ||
                x.voornaam.toLowerCase().includes((event.query as String).toLowerCase()),
        );
    }

    addDienst() {
        let newDienst = new Dienst();
        this.bewerkteDiensten.push(newDienst);
        this.bewerkteDiensten = _.clone(this.bewerkteDiensten);
    }

    deleteDienst(diensten: Dienst[]) {
        diensten.forEach(dienst => {
            this.bewerkteDiensten = this.bewerkteDiensten.filter(val => val !== dienst);
        });
        this.geselecteerdeDiensten = [];
        this.bewerkteDiensten = _.clone(this.bewerkteDiensten);
    }

    duplicateDienst(dienst: Dienst) {
        let cp: Dienst = _.clone(dienst);
        cp.id = null;
        cp.dataKey = Math.random();
        cp.onderbrekingen = [];
        dienst.onderbrekingen.forEach(t => {
            let cpOnderbreking = _.clone(t);
            cpOnderbreking.dataKey = Math.random();
            cpOnderbreking.id = null;
            cp.onderbrekingen.push(cpOnderbreking);
        });
        this.bewerkteDiensten.push(cp);
        this.bewerkteDiensten = _.clone(this.bewerkteDiensten);
    }

    getAllDiensten() {
        this.dienstService.getAllDiensten$().subscribe(
            val => {
                if (val) {
                    this.bewerkteDiensten = val;
                    this.verkregenDiensten = _.cloneDeep(this.bewerkteDiensten);
                    this.loadingDiensten = false;
                }
            },
            error => {
                this.messageService.clear();
                if (error.error instanceof ProgressEvent) {
                    this.messageService.add({severity: 'error', summary: 'De databank is onbereikbaar!'});
                } else {
                    console.log(error);
                    this.messageService.add({severity: 'error', summary: error.error, life: 10000});
                }
                this.loadingDiensten = false;
            },
        );
    }

    changeDag(item: Dienst, event, dag) {
        if (item.startDag == null) {
            item.startDag = event.id;
        } else if (item.eindDag == null) {
            item.eindDag = event.id;
        }
        if (dag == 'startDag') {
            item.onderbrekingen.forEach(t => (t.startDag = event.id));
        } else if (dag == 'eindDag') {
            item.onderbrekingen.forEach(t => (t.eindDag = event.id));
        }
    }

    checkTime(time: string, elementId?: string): number {
        var uitvoer;
        try {
            if (time == '' || time.match('^[0-2][0-9]:[0-5][0-9]$') == null || parseInt(time.split(':')[0]) >= 24) {
                uitvoer = '00:00';
            } else {
                uitvoer = time;
            }
        } catch (e) {
            uitvoer = 0;
        }
        try {
            if (elementId != null) {
                (document.getElementById(elementId) as HTMLInputElement).value = uitvoer;
            }
        } catch (e) {}
        return uitvoer;
    }
}
