import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import * as moment from 'moment';
import {MessageService} from 'primeng/api';
import * as XLSX from 'xlsx';
import {BusChauffeur} from '../modals/bus-chauffeur';
import {DagExport} from '../modals/dag-export';
import {EffectieveDienst} from '../modals/effectieve-dienst';
import {Feestdag} from '../modals/feestdag';
import {Instellingen} from '../modals/instellingen';
import {Stelsels} from '../modals/stelsels.enum';
import {EffectieveDienstService} from '../services/effectieve-dienst.service';
import {FeestdagenService} from '../services/feestdagen.service';

@Component({
    selector: 'app-toon-loonlijst',
    templateUrl: './toon-loonlijst.component.html',
    styleUrls: ['./toon-loonlijst.component.scss'],
})
export class ToonLoonlijstComponent implements OnInit {
    /*//https://medium.com/@madhavmahesh/exporting-an-excel-file-in-angular-927756ac9857*/
    public busChauffeur: BusChauffeur;
    public maand: moment.Moment = moment().month(this.route.snapshot.params.maand - 1);
    public feestdagen: Feestdag[] = [];
    public loading = true;
    public effectieveDiensten: EffectieveDienst[];
    public data: DagExport[];
    public instellingen: Instellingen;
    public dataTotaal: any[];
    public stelsels = Stelsels;
    private dayNames = ['Zo', 'Ma', 'Di', 'Woe', 'Do', 'Vrij', 'Za'];
    public totaal: DagExport;

    constructor(
        private messageService: MessageService,
        private router: Router,
        private route: ActivatedRoute,
        public feestdagenService: FeestdagenService,
        public effectieveDienstenService: EffectieveDienstService,
    ) {
        this.route.data.subscribe(data => {
            this.busChauffeur = data.busChauffeur;
            this.effectieveDiensten = data.effectieveDiensten;
            this.instellingen = data.instellingen;
        });
    }

    ngOnInit() {
        this.loadFeestdagen();
    }

    loadFeestdagen() {
        this.feestdagenService.getAllFeestdagen$().subscribe(
            val => {
                if (val) {
                    this.feestdagen = val;
                    this.loadLijst();
                    this.loading = false;
                }
            },
            error => {
                this.messageService.clear();
                if (error.error instanceof ProgressEvent) {
                    this.messageService.add({severity: 'error', summary: 'De databank is onbereikbaar!'});
                } else {
                    console.log(error);
                    this.messageService.add({severity: 'error', summary: error.error, life: 10000});
                }
                this.loading = false;
            },
        );
    }

    veranderHuidigeMaand(met: number) {
        this.loading = true;
        if (met < 0) {
            this.maand.subtract(1, 'months');
        } else {
            this.maand.add(1, 'months');
        }
        this.effectieveDienstenService.getEffectieveDienstenByMonth$(this.maand.year(), this.maand.month() + 1, this.busChauffeur.id).subscribe(
            val => {
                if (val) {
                    this.effectieveDiensten = val;
                    this.loading = false;
                    this.loadLijst();
                }
            },
            error => {
                this.messageService.clear();
                if (error.error instanceof ProgressEvent) {
                    this.messageService.add({severity: 'error', summary: 'De databank is onbereikbaar!'});
                } else {
                    console.log(error);
                    this.messageService.add({severity: 'error', summary: error.error, life: 10000});
                }
                this.loading = false;
            },
        );
    }

    loadLijst() {
        const monthIndex = this.maand.month();
        const date = new Date(this.maand.year(), monthIndex, 1);
        const dagExports: DagExport[] = [];
        while (date.getMonth() === monthIndex) {
            const dagExport = new DagExport(date, this.maand.format('MM'), this.dayNames[date.getDay()], this.instellingen);
            dagExports.push(dagExport);
            date.setDate(date.getDate() + 1);
        }
        this.effectieveDiensten.forEach(dienst => {
            if (dienst.gerelateerdeDienst === null || dienst.start < new Date(dienst.gerelateerdeDienst.start)) {
                const dienstenInExport = dagExports[dienst.start.getDate() - 1].effectieveDiensten.value;
                dienstenInExport.push(dienst);
                dagExports[dienst.start.getDate() - 1].effectieveDiensten.next(dienstenInExport);
            }
        });
        this.feestdagen
            .filter(x => x.dag.getMonth() == this.maand.month())
            .forEach(fd => {
                const dagExportVoorFeestdag = dagExports.find(x => x.dagNummer == fd.dag.getDate());
                const feestdagen = dagExportVoorFeestdag.feestdagen.value;
                feestdagen.push(fd);
                dagExportVoorFeestdag.feestdagen.next(feestdagen);
            });
        this.data = dagExports;
        this.setTotaal();
    }

    setTotaal(): any {
        const result = new DagExport(new Date(), this.maand.format('MM'), this.dayNames[new Date().getDay()], this.instellingen);
        this.data.forEach(basket => {
            for (const [key, value] of Object.entries(basket)) {
                if (result[key]) {
                    result[key] += value;
                } else {
                    result[key] = value;
                }
            }
        });
        this.totaal = result;
    }

    getHuidigeMaand() {
        return `${this.maand.format('MMMM').charAt(0).toUpperCase()}${this.maand.format('MMMM').substring(1)} ${this.maand.year()}`;
    }

    printLoonlijst() {
        // this.excelService.exportAsExcelFile(this.data, 'sample');
        const element = document.getElementById('afdrukken');
        const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);
        const wb: XLSX.WorkBook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
        XLSX.writeFile(wb, `${this.busChauffeur.voornaam}_${this.busChauffeur.achternaam}_${this.getHuidigeMaand()}_loonlijst.xlsx`);
    }

    msToTime(s: number = 0): string {
        const ms = s % 1000;
        s = (s - ms) / 1000;
        const secs = s % 60;
        s = (s - secs) / 60;
        const mins = s % 60;
        const hrs = (s - mins) / 60;
        let uitvoer = '';
        if (s >= 0) {
            if (hrs.toString().length < 2) {
                uitvoer = `0${hrs}:`;
            } else {
                uitvoer = `${hrs}:`;
            }
            if (mins.toString().length < 2) {
                uitvoer += `0${mins}`;
            } else {
                uitvoer += `${mins}`;
            }
        } else {
            if (hrs.toString().length < 3) {
                uitvoer = `-0${Math.abs(hrs)}:`;
            } else {
                uitvoer = `${hrs}:`;
            }
            if (Math.abs(mins).toString().length < 2) {
                uitvoer += `0${Math.abs(mins)}`;
            } else {
                uitvoer += `${Math.abs(mins)}`;
            }
        }
        return uitvoer;
    }

    minutesToTime(value: number): string {
        const hours = Math.floor((value * 60) / 3600);
        const minutes = value % 60;
        const hoursUitvoer = hours.toString().length === 1 ? 0 + hours.toString() : hours.toString();
        const minutesUitvoer = minutes.toString().length === 1 ? 0 + minutes.toString() : minutes.toString();
        return hoursUitvoer + ':' + minutesUitvoer;
    }

    redirectTo(voorvoegsel: string, bc: any) {
        this.router.navigate([`../${voorvoegsel}/${bc.id}`]);
    }
}
