import {Component, OnInit} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import * as _ from 'lodash';
import {MenuItem, MessageService} from 'primeng/api';
import {TabMenu} from 'primeng/tabmenu';
import {Instellingen} from '../modals/instellingen';
import {InstellingenService} from '../services/instellingen.service';
@Component({
    selector: 'app-instellingen-aanpassen',
    templateUrl: './instellingen-aanpassen.component.html',
    styleUrls: ['./instellingen-aanpassen.component.scss'],
})
export class InstellingenAanpassenComponent implements OnInit {
    public bewerkteInstellingen: Instellingen;
    public verkregenInstellingen: Instellingen;
    constructor(
        private messageService: MessageService,
        public route: ActivatedRoute,
        public router: Router,
        private fb: FormBuilder,
        public instellingenService: InstellingenService,
    ) {}

    items: MenuItem[];
    activeItem: MenuItem;
    kolommen = [
        {field: 'voornaam', header: 'Voornaam'},
        {field: 'achternaam', header: 'Achternaam'},
        {field: 'uurloon', header: 'Uurloon'},
        {field: 'geboorteDatum', header: 'Geboortedatum'},
        {field: 'straatnaam', header: 'Straatnaam'},
        {field: 'huisnummer', header: 'Huisnummer'},
        {field: 'postcode', header: 'Postcode'},
        {field: 'stad', header: 'Stad'},
        {field: 'land', header: 'Land'},
        {field: 'afstandKM', header: 'Afstand(KM)'},
        {field: 'stelsel.id', header: 'Stelsel'},
        {field: 'overUren', header: 'Overuren'},
        {field: 'aantalDagenJaarlijksVerlof', header: 'AantaldagenJV'},
    ];
    ngOnInit() {
        this.getInstellingen();
        this.items = [
            {label: 'Algemeen', icon: 'pi pi-fw pi-briefcase'},
            {label: 'Loonlijst', icon: 'pi pi-fw pi-file-o'},
            {label: 'Loonfiche', icon: 'pi pi-fw pi-table'},
        ];
        this.activeItem = this.items[0];
    }

    getInstellingen() {
        this.instellingenService.getInstellingen$().subscribe(
            val => {
                if (val) {
                    this.bewerkteInstellingen = val;
                    this.verkregenInstellingen = _.cloneDeep(this.bewerkteInstellingen);
                }
            },
            error => {
                this.messageService.clear();
                if (error.error instanceof ProgressEvent) {
                    this.messageService.add({severity: 'error', summary: 'De databank is onbereikbaar!'});
                } else {
                    console.log(error);
                    this.messageService.add({severity: 'error', summary: error.error, life: 10000});
                }
            },
        );
    }

    activateMenu(tab: TabMenu) {
        this.activeItem = tab.activeItem;
    }

    setBcTabelCols(item) {
        if (!_.isEqual(item, this.bewerkteInstellingen.defaultGeselecteerdeKolommenBijBuschauffeurTabel)) {
            this.bewerkteInstellingen.localBuschauffeurTabel = item;
        }
    }

    instellingenOpslaan() {
        this.instellingenService.putInstellingen$(this.bewerkteInstellingen).subscribe(
            val => {
                if (val) {
                    this.bewerkteInstellingen = val;
                    this.verkregenInstellingen = _.cloneDeep(this.bewerkteInstellingen);
                    this.messageService.add({severity: 'success', summary: 'Instellingen opgeslaan!'});
                }
            },
            error => {
                console.log(error);
                this.messageService.clear();
                if (error.error instanceof ProgressEvent) {
                    this.messageService.add({severity: 'error', summary: 'De databank is onbereikbaar!'});
                } else if (typeof error.error == 'string') {
                    this.messageService.add({severity: 'error', summary: error.error, life: 10000});
                } else if (typeof error.error == 'object') {
                    for (let key in error.error.errors) {
                        let errorToShow = error.error.errors[key][0];
                        if (errorToShow != null) {
                            this.messageService.add({severity: 'error', summary: errorToShow, life: 10000});
                        } else {
                            this.messageService.add({severity: 'error', summary: 'Er liep iets fout!', life: 10000});
                        }
                    }
                } else {
                    this.messageService.add({severity: 'error', summary: 'Er liep iets fout!', life: 10000});
                }
            },
        );
    }

    msToTime(s: number = 0): string {
        const ms = s % 1000;
        s = (s - ms) / 1000;
        const secs = s % 60;
        s = (s - secs) / 60;
        const mins = s % 60;
        const hrs = (s - mins) / 60;
        let uitvoer = '';
        if (s >= 0) {
            if (hrs.toString().length < 2) {
                uitvoer = `0${hrs}:`;
            } else {
                uitvoer = `${hrs}:`;
            }
            if (mins.toString().length < 2) {
                uitvoer += `0${mins}`;
            } else {
                uitvoer += `${mins}`;
            }
        } else {
            if (hrs.toString().length < 3) {
                uitvoer = `-0${Math.abs(hrs)}:`;
            } else {
                uitvoer = `${hrs}:`;
            }
            if (Math.abs(mins).toString().length < 2) {
                uitvoer += `0${Math.abs(mins)}`;
            } else {
                uitvoer += `${Math.abs(mins)}`;
            }
        }
        return uitvoer;
    }

    timeToMs(time: string, elementId?: string): number {
        var uitvoer;
        try {
            if (time == '' || time.match('[-]?[0-9]*:[0-5][0-9]$') == null) {
                uitvoer = 0;
            } else {
                const splittedTime = time.toString().split(':');
                if (time.substring(0, 1) != '-') {
                    uitvoer = 3600000 * parseInt(splittedTime[0]) + 60000 * parseInt(splittedTime[1]);
                } else {
                    uitvoer = 3600000 * parseInt(splittedTime[0]) - 60000 * parseInt(splittedTime[1]);
                }
            }
        } catch (e) {
            uitvoer = 0;
        }
        try {
            if (elementId != null) {
                (document.getElementById(elementId) as HTMLInputElement).value = this.msToTime(uitvoer);
            }
        } catch (e) {}
        return uitvoer;
    }

    compare() {
        return _.isEqual(this.bewerkteInstellingen, this.verkregenInstellingen);
    }
}
