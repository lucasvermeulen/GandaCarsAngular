import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {Observable, throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {environment} from 'src/environments/environment';
import {LoonficheSetting} from '../modals/loonfiche-setting';

@Injectable({
    providedIn: 'root',
})
export class LoonficheSettingService {
    constructor(private router: Router, private http: HttpClient) {}

    getLoonficheSettingById$(id: string): Observable<LoonficheSetting> {
        return this.http.get<LoonficheSetting>(`${environment.apiUrl}/loonficheSetting/${id}`).pipe(
            catchError(error => {
                return throwError(error);
            }),
            map(
                (lfs: any): LoonficheSetting => {
                    lfs = LoonficheSetting.fromJSON(lfs);
                    return lfs;
                },
            ),
        );
    }

    getLoonficheSettingByJaarEnMaand$(busChaffeurId: string, jaar: number, maand: number): Observable<LoonficheSetting> {
        return this.http.get<LoonficheSetting>(`${environment.apiUrl}/loonficheSetting/${busChaffeurId}/${jaar}/${maand}`).pipe(
            catchError(error => {
                return throwError(error);
            }),
            map(
                (lfs: any): LoonficheSetting => {
                    lfs = LoonficheSetting.fromJSON(lfs);
                    return lfs;
                },
            ),
        );
    }

    addLoonficheSetting$(loonficheSetting: LoonficheSetting): Observable<LoonficheSetting> {
        return this.http
            .post<LoonficheSetting>(`${environment.apiUrl}/loonficheSetting`, loonficheSetting, {responseType: 'json'})
            .pipe(
                catchError(error => {
                    return throwError(error);
                }),
                map(
                    (lfs: any): LoonficheSetting => {
                        lfs = LoonficheSetting.fromJSON(lfs);
                        return lfs;
                    },
                ),
            );
    }

    putLoonficheSetting$(lfs: LoonficheSetting): Observable<LoonficheSetting> {
        return this.http
            .put<LoonficheSetting>(`${environment.apiUrl}/loonficheSetting/${lfs.id}`, lfs, {responseType: 'json'})
            .pipe(
                catchError(error => {
                    return throwError(error);
                }),
                map(
                    (lfs: any): LoonficheSetting => {
                        lfs = LoonficheSetting.fromJSON(lfs);
                        return lfs;
                    },
                ),
            );
    }

    deleteLoonficheSetting$(lfs: LoonficheSetting): Observable<any> {
        return this.http.delete<LoonficheSetting>(`${environment.apiUrl}/loonficheSetting/${lfs.id}`).pipe(
            catchError(error => {
                return throwError(error);
            }),
            map((lfs: any): any => {
                lfs = LoonficheSetting.fromJSON(lfs);
                return lfs;
            }),
        );
    }

    getAllLoonficheSettings$(): Observable<LoonficheSetting[]> {
        return this.http.get<LoonficheSetting[]>(`${environment.apiUrl}/loonficheSetting/getAll`).pipe(
            catchError(error => {
                return throwError(error);
            }),
            map((list: any[]): any[] => {
                list = list.map(LoonficheSetting.fromJSON);
                return list;
            }),
        );
    }
}
