import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {Observable, throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {environment} from 'src/environments/environment';
import {BusChauffeur} from '../modals/bus-chauffeur';
import {EffectieveDienst} from '../modals/effectieve-dienst';

@Injectable({
    providedIn: 'root',
})
export class EffectieveDienstService {
    constructor(private router: Router, private http: HttpClient) {}

    getEffectievieDienstenUndefinedDienstenNaarEffectieveDiensten$(jaar: string, week: string, id: String): Observable<EffectieveDienst[]> {
        return this.http
            .get(`${environment.apiUrl}/EffectieveDiensten/GetEffectieveDienstenNullDienstenNaarEffectieveDiensten/${jaar}/${week}/${id}`)
            .pipe(
                catchError(error => {
                    return throwError(error);
                }),
                map((list: any[]): any[] => {
                    list = list.map(x => EffectieveDienst.fromJSON(x));
                    return list;
                }),
            );
    }

    getEffectieveDiensten$(jaar: string, week: string, busChauffeur: BusChauffeur): Observable<EffectieveDienst[]> {
        return this.http.get(`${environment.apiUrl}/EffectieveDiensten/${jaar}/${week}/${busChauffeur.id}`).pipe(
            catchError(error => {
                return throwError(error);
            }),
            map((list: any[]): any[] => {
                list = list.map(x => EffectieveDienst.fromJSON(x));
                return list;
            }),
        );
    }

    getEffectieveDienstenByMonth$(jaar: number, maand: number, busChauffeurId: string): Observable<EffectieveDienst[]> {
        return this.http.get(`${environment.apiUrl}/EffectieveDiensten/getByMonth/${jaar}/${maand}/${busChauffeurId}`).pipe(
            catchError(error => {
                return throwError(error);
            }),
            map((list: any[]): any[] => {
                list = list.map(x => EffectieveDienst.fromJSON(x));
                return list;
            }),
        );
    }

    postEffectieveDiensten$(jaar: string, week: string, busChauffeur: BusChauffeur, ed: EffectieveDienst[]): Observable<EffectieveDienst[]> {
        return this.http.post(`${environment.apiUrl}/EffectieveDiensten/${jaar}/${week}/${busChauffeur.id}`, ed).pipe(
            catchError(error => {
                return throwError(error);
            }),
            map((list: any[]): any[] => {
                list = list.map(x => EffectieveDienst.fromJSON(x));
                return list;
            }),
        );
    }
    deleteEffectieveDiensten$(jaar: string, week: string, busChauffeur: BusChauffeur): Observable<EffectieveDienst[]> {
        return this.http.delete(`${environment.apiUrl}/EffectieveDiensten/verwijderDiensten/${jaar}/${week}/${busChauffeur.id}`).pipe(
            catchError(error => {
                return throwError(error);
            }),
            map((list: any[]): any[] => {
                list = list.map(x => EffectieveDienst.fromJSON(x));
                return list;
            }),
        );
    }
}
