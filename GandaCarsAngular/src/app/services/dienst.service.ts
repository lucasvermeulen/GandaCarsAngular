import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {forkJoin, Observable, throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {environment} from 'src/environments/environment';
import {Dienst} from '../modals/dienst';

@Injectable({
    providedIn: 'root',
})
export class DienstService {
    constructor(private router: Router, private http: HttpClient) {}

    tryGetDienstByName(name: string): Observable<Dienst[]> {
        return this.http.get(`${environment.apiUrl}/Dienst/tryGetByName/${name}`).pipe(
            catchError(error => {
                return throwError(error);
            }),
            map((list: any[]): any[] => {
                list = list.map(Dienst.fromJSON);
                return list;
            }),
        );
    }

    getDienstById$(id: string): Observable<Dienst> {
        return this.http.get(`${environment.apiUrl}/Dienst/${id}`).pipe(
            catchError(error => {
                return throwError(error);
            }),
            map(
                (d: any): Dienst => {
                    d = Dienst.fromJSON(d);
                    return d;
                },
            ),
        );
    }

    addDienst$(dienst: Dienst): Observable<Dienst> {
        return this.http
            .post<Dienst>(`${environment.apiUrl}/Dienst`, dienst, {responseType: 'json'})
            .pipe(
                catchError(error => {
                    return throwError(error);
                }),
                map(
                    (d: any): Dienst => {
                        d = Dienst.fromJSON(d);
                        return d;
                    },
                ),
            );
    }

    updateCrud(dienstenToAdd: Dienst[], dienstenToPut: Dienst[], dienstenToDelete: Dienst[]): Observable<any> {
        const uitTeVoeren = [];
        if (dienstenToDelete.length > 0) {
            dienstenToDelete.forEach(dienst => {
                uitTeVoeren.push(this.deleteDienst$(dienst));
            });
        }
        if (dienstenToAdd.length > 0) {
            dienstenToAdd.forEach(dienst => {
                uitTeVoeren.push(this.addDienst$(dienst));
            });
        }
        if (dienstenToPut.length > 0) {
            dienstenToPut.forEach(dienst => {
                uitTeVoeren.push(this.putDienst$(dienst));
            });
        }
        return forkJoin(uitTeVoeren).pipe(
            catchError(error => {
                return throwError(error);
            }),
            map((list: any[]): any[] => {
                return list;
            }),
        );
    }

    deleteDienst$(d: Dienst): Observable<Dienst> {
        return this.http.delete<Dienst>(`${environment.apiUrl}/Dienst/${d.id}`).pipe(
            catchError(error => {
                return throwError(error);
            }),
            map(
                (item: any): Dienst => {
                    item = Dienst.fromJSON(item);
                    return item;
                },
            ),
        );
    }

    putDienst$(dienst: Dienst): Observable<Dienst> {
        return this.http
            .put<Dienst>(`${environment.apiUrl}/Dienst/${dienst.id}`, dienst, {responseType: 'json'})
            .pipe(
                catchError(error => {
                    return throwError(error);
                }),
                map(
                    (item: any): Dienst => {
                        item = Dienst.fromJSON(item);
                        return item;
                    },
                ),
            );
    }

    getAllDiensten$(): Observable<Dienst[]> {
        return this.http.get<Dienst[]>(`${environment.apiUrl}/Dienst/getAll`).pipe(
            catchError(error => {
                return throwError(error);
            }),
            map((list: any[]): any[] => {
                list = list.map(Dienst.fromJSON);
                return list;
            }),
        );
    }
}
