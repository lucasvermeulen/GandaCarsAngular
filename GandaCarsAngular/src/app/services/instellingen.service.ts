import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import * as moment from 'moment';
import {Observable, throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {environment} from 'src/environments/environment';
import {Instellingen} from '../modals/instellingen';
@Injectable({
    providedIn: 'root',
})
export class InstellingenService {
    public instellingen: Instellingen;
    private timeLeft: number = 1800000;
    private interval;
    public timerTijd: string = null;

    constructor(private router: Router, private http: HttpClient) {
        this.checkForTimerToHaveFinished();
    }

    checkForTimerToHaveFinished() {
        var timerStarted = localStorage.getItem('timerStarted');
        if (timerStarted != null && moment(new Date(timerStarted)).diff(moment(new Date())) >= 1800000) {
            this.changeOverUrenToFalse();
        } else if (timerStarted != null && this.interval == null) {
            this.timeLeft = 1800000 - moment(new Date()).diff(moment(new Date(timerStarted)));
            this.startTimer();
        }
    }

    changeOverUrenToFalse() {
        localStorage.removeItem('timerStarted');
        if (this.instellingen != null) {
            this.instellingen.overurenAanpassen = false;
            this.putInstellingen$(this.instellingen).subscribe();
        }
        this.timeLeft = 1800000;
        this.timerTijd = null;
        clearInterval(this.interval);
    }
    startTimer() {
        var timerStarted = localStorage.getItem('timerStarted');
        if (timerStarted === null) {
            localStorage.setItem('timerStarted', new Date().toString());
        }
        this.interval = setInterval(() => {
            if (this.timeLeft > 0) {
                this.timeLeft -= 1000;
                this.timerTijd = this.msToTime(this.timeLeft);
            } else {
                this.changeOverUrenToFalse();
            }
        }, 1000);
    }

    getInstellingen$(): Observable<Instellingen> {
        this.checkForTimerToHaveFinished();
        return this.http.get<Instellingen>(`${environment.apiUrl}/Instellingen`).pipe(
            catchError(error => {
                return throwError(error);
            }),
            map(
                (item: any): Instellingen => {
                    item = Instellingen.fromJSON(item);
                    this.instellingen = item;
                    return item;
                },
            ),
        );
    }

    putInstellingen$(instellingen: Instellingen): Observable<Instellingen> {
        if (instellingen.overurenAanpassen == true) {
            this.startTimer();
        } else if (this.interval != null) {
            localStorage.removeItem('timerStarted');
            clearInterval(this.interval);
            this.timeLeft = 1800000;
            this.timerTijd = null;
        }
        this.checkForTimerToHaveFinished();
        return this.http
            .put<Instellingen>(`${environment.apiUrl}/Instellingen`, instellingen, {responseType: 'json'})
            .pipe(
                catchError(error => {
                    return throwError(error);
                }),
                map(
                    (item: any): Instellingen => {
                        item = Instellingen.fromJSON(item);
                        this.instellingen = item;
                        return item;
                    },
                ),
            );
    }

    msToTime(s: number = 0): string {
        const ms = s % 1000;
        s = (s - ms) / 1000;
        const secs = s % 60;
        s = (s - secs) / 60;
        const mins = s % 60;
        let uitvoer = '';
        if (s >= 0) {
            if (mins.toString().length < 2) {
                uitvoer = `0${mins}:`;
            } else {
                uitvoer = `${mins}:`;
            }
            if (secs.toString().length < 2) {
                uitvoer += `0${secs}`;
            } else {
                uitvoer += `${secs}`;
            }
        } else {
            if (mins.toString().length < 3) {
                uitvoer = `-0${Math.abs(mins)}:`;
            } else {
                uitvoer = `${mins}:`;
            }
            if (Math.abs(secs).toString().length < 2) {
                uitvoer += `0${Math.abs(secs)}`;
            } else {
                uitvoer += `${Math.abs(secs)}`;
            }
        }
        return uitvoer;
    }
}
