import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {forkJoin, Observable, throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {environment} from 'src/environments/environment';
import {BusChauffeur} from '../modals/bus-chauffeur';

@Injectable({
    providedIn: 'root',
})
export class BusChauffeurService {
    constructor(private router: Router, private http: HttpClient) {}

    getBusChauffeurById$(id: string): Observable<BusChauffeur> {
        return this.http.get<BusChauffeur>(`${environment.apiUrl}/busChauffeur/${id}`).pipe(
            catchError(error => {
                return throwError(error);
            }),
            map(
                (bc: any): BusChauffeur => {
                    bc = BusChauffeur.fromJSON(bc);
                    return bc;
                },
            ),
        );
    }

    addBusChauffeur$(busChauffeur: BusChauffeur): Observable<BusChauffeur> {
        return this.http
            .post<BusChauffeur>(`${environment.apiUrl}/BusChauffeur`, busChauffeur, {responseType: 'json'})
            .pipe(
                catchError(error => {
                    return throwError(error);
                }),
                map(
                    (bc: any): BusChauffeur => {
                        bc = BusChauffeur.fromJSON(bc);
                        return bc;
                    },
                ),
            );
    }

    putBusChauffeur$(bc: BusChauffeur): Observable<BusChauffeur> {
        return this.http
            .put<BusChauffeur>(`${environment.apiUrl}/BusChauffeur/${bc.id}`, bc, {responseType: 'json'})
            .pipe(
                catchError(error => {
                    return throwError(error);
                }),
                map(
                    (bc: any): BusChauffeur => {
                        bc = BusChauffeur.fromJSON(bc);
                        return bc;
                    },
                ),
            );
    }

    deleteBusChauffeur$(bc: BusChauffeur): Observable<BusChauffeur> {
        return this.http.delete<BusChauffeur>(`${environment.apiUrl}/BusChauffeur/${bc.id}`).pipe(
            catchError(error => {
                return throwError(error);
            }),
            map(
                (item: any): BusChauffeur => {
                    item = BusChauffeur.fromJSON(item);
                    return item;
                },
            ),
        );
    }

    getAllBusChauffeurs$(): Observable<BusChauffeur[]> {
        return this.http.get<BusChauffeur[]>(`${environment.apiUrl}/BusChauffeur/getAll`).pipe(
            catchError(error => {
                return throwError(error);
            }),
            map((list: any[]): any[] => {
                list = list.map(BusChauffeur.fromJSON);
                return list;
            }),
        );
    }

    updateCrud(chauffeursToAdd: BusChauffeur[], chauffeursToPut: BusChauffeur[], chauffeursToDelete: BusChauffeur[]): Observable<any> {
        const uitTeVoeren = [];
        if (chauffeursToAdd.length > 0) {
            chauffeursToAdd.forEach(busChauffeur => {
                uitTeVoeren.push(this.addBusChauffeur$(busChauffeur));
            });
        }
        if (chauffeursToPut.length > 0) {
            chauffeursToPut.forEach(busChauffeur => {
                uitTeVoeren.push(this.putBusChauffeur$(busChauffeur));
            });
        }
        if (chauffeursToDelete.length > 0) {
            chauffeursToDelete.forEach(busChauffeur => {
                uitTeVoeren.push(this.deleteBusChauffeur$(busChauffeur));
            });
        }
        return forkJoin(uitTeVoeren).pipe(
            catchError(error => {
                return throwError(error);
            }),
            map((list: any[]): any[] => {
                return list;
            }),
        );
    }
}
