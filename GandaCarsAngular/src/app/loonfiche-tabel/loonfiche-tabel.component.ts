import {Component, Input, OnInit} from '@angular/core';
import * as _ from 'lodash';
import {LoonficheTabel} from '../modals/loonfiche-tabel';

@Component({
    selector: 'app-loonfiche-tabel',
    templateUrl: './loonfiche-tabel.component.html',
    styleUrls: ['./loonfiche-tabel.component.scss'],
})
export class LoonficheTabelComponent implements OnInit {
    @Input()
    loonficheTabel: LoonficheTabel;
    @Input()
    public loading: boolean;
    constructor() {}

    ngOnInit(): void {}

    getFirstDayOfData() {
        return _.orderBy(this.loonficheTabel.dagExports, ['dagNummer'], ['asc'])[0].datum;
    }
    getLastDayOfData() {
        return _.orderBy(this.loonficheTabel.dagExports, ['dagNummer'], ['asc'])[this.loonficheTabel.dagExports.length - 1].datum;
    }
    msToTime(s: number = 0): string {
        const ms = s % 1000;
        s = (s - ms) / 1000;
        const secs = s % 60;
        s = (s - secs) / 60;
        const mins = s % 60;
        const hrs = (s - mins) / 60;
        let uitvoer = '';
        if (s >= 0) {
            if (hrs.toString().length < 2) {
                uitvoer = `0${hrs}:`;
            } else {
                uitvoer = `${hrs}:`;
            }
            if (mins.toString().length < 2) {
                uitvoer += `0${mins}`;
            } else {
                uitvoer += `${mins}`;
            }
        } else {
            if (hrs.toString().length < 3) {
                uitvoer = `-0${Math.abs(hrs)}:`;
            } else {
                uitvoer = `${hrs}:`;
            }
            if (Math.abs(mins).toString().length < 2) {
                uitvoer += `0${Math.abs(mins)}`;
            } else {
                uitvoer += `${Math.abs(mins)}`;
            }
        }
        return uitvoer;
    }
}
