import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import * as _ from 'lodash';
import {MessageService} from 'primeng/api';
import {BusChauffeur} from '../modals/bus-chauffeur';
import {Instellingen} from '../modals/instellingen';
import {Stelsels} from '../modals/stelsels.enum';
import {BusChauffeurService} from '../services/bus-chauffeur.service';
import {InstellingenService} from '../services/instellingen.service';
@Component({
    selector: 'app-bus-chauffeur-overzicht',
    templateUrl: './bus-chauffeur-overzicht.component.html',
    styleUrls: ['./bus-chauffeur-overzicht.component.scss'],
})
export class BusChauffeurOverzichtComponent implements OnInit {
    public loadingBusChauffeurs = true;
    public verkregenBusChauffeurs: BusChauffeur[];
    public bewerkteBusChauffeurs: BusChauffeur[];
    public geselecteerdeChauffeurs: BusChauffeur[];
    public stelsels = Stelsels;
    public instellingen: Instellingen;
    kolommen: any[];
    _geselecteerdeKolommen: any[];
    public locale = {
        firstDayOfWeek: 1,
        dayNames: ['Zondag', 'Maandag', 'Dinsdag', 'Woensdag', 'Donderdag', 'Vrijdag', 'Zaterdag'],
        dayNamesShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
        dayNamesMin: ['Zo', 'Ma', 'Di', 'Woe', 'Do', 'Vr', 'Za'],
        monthNames: ['Januari', 'Februari', 'Maart', 'April', 'Mei', 'Juni', 'Juli', 'Augustus', 'September', 'Oktober', 'November', 'December'],
        monthNamesShort: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dec'],
        today: 'Vandaag',
        clear: 'Clear',
        dateFormat: 'dd/mm/yy',
        weekHeader: 'Wk',
    };
    constructor(
        private messageService: MessageService,
        private router: Router,
        private busChauffeurService: BusChauffeurService,
        private instellingsService: InstellingenService,
    ) {}

    ngOnInit() {
        this.getAllBuschauffeurs();
        this.getInstellingen();
        this.kolommen = [
            {field: 'voornaam', header: 'Voornaam'},
            {field: 'achternaam', header: 'Achternaam'},
            {field: 'uurloon', header: 'Uurloon'},
            {field: 'geboorteDatum', header: 'Geboortedatum'},
            {field: 'straatnaam', header: 'Straatnaam'},
            {field: 'huisnummer', header: 'Huisnummer'},
            {field: 'postcode', header: 'Postcode'},
            {field: 'stad', header: 'Stad'},
            {field: 'land', header: 'Land'},
            {field: 'afstandKM', header: 'Afstand (KM)'},
            {field: 'stelsel.id', header: 'Stelsel'},
            {field: 'overUren', header: 'Overuren'},
            {field: 'aantalDagenJaarlijksVerlof', header: 'Aantal dagen JV'},
        ];
        this.geselecteerdeKolommen = this.kolommen;
    }

    kolomGeselecteerd(field: string) {
        return this._geselecteerdeKolommen.find(x => x.field == field) != null;
    }

    @Input() get geselecteerdeKolommen(): any[] {
        return this._geselecteerdeKolommen;
    }

    set geselecteerdeKolommen(val: any[]) {
        this._geselecteerdeKolommen = this.kolommen.filter(col => val.map(x => x.field).includes(col.field));
    }

    wijzigingenOngedaanMaken() {
        this.bewerkteBusChauffeurs = _.cloneDeep(this.verkregenBusChauffeurs);
    }

    buschauffeursOpslaan() {
        this.loadingBusChauffeurs = true;
        let chauffeursToDelete = [];
        let chauffeursToEdit: BusChauffeur[] = [];
        this.verkregenBusChauffeurs.forEach(oudeChauffeur => {
            if (this.bewerkteBusChauffeurs.find(x => _.isEqual(x, oudeChauffeur)) == undefined) {
                let bewerkteChauffeur = this.bewerkteBusChauffeurs.find(x => x.id === oudeChauffeur.id);
                if (bewerkteChauffeur != undefined) {
                    chauffeursToEdit.push(bewerkteChauffeur);
                } else {
                    chauffeursToDelete.push(oudeChauffeur);
                }
            }
        });
        let chauffeursToAdd = this.bewerkteBusChauffeurs.filter(x => x.id == null);
        this.busChauffeurService.updateCrud(chauffeursToAdd, chauffeursToEdit, chauffeursToDelete).subscribe(
            val => {
                if (val) {
                    this.getAllBuschauffeurs();
                    this.loadingBusChauffeurs = false;
                    this.messageService.add({severity: 'success', summary: 'De buschauffeurs werden opgeslaan!', life: 10000});
                }
            },
            error => {
                console.log(error);
                this.messageService.clear();
                if (error.error instanceof ProgressEvent) {
                    this.messageService.add({severity: 'error', summary: 'De databank is onbereikbaar!'});
                } else if (typeof error.error == 'string') {
                    this.messageService.add({severity: 'error', summary: error.error, life: 10000});
                } else if (typeof error.error == 'object') {
                    for (let key in error.error.errors) {
                        let errorToShow = error.error.errors[key][0];
                        if (errorToShow != null) {
                            this.messageService.add({severity: 'error', summary: errorToShow, life: 10000});
                        } else {
                            this.messageService.add({severity: 'error', summary: 'Er liep iets fout!', life: 10000});
                        }
                    }
                } else {
                    this.messageService.add({severity: 'error', summary: 'Er liep iets fout!', life: 10000});
                }
                this.loadingBusChauffeurs = false;
            },
        );
    }

    addBuschauffeur() {
        this.geselecteerdeKolommen = this.kolommen;
        this.bewerkteBusChauffeurs.push(new BusChauffeur());
        this.bewerkteBusChauffeurs = _.clone(this.bewerkteBusChauffeurs);
    }

    deleteBuschauffeur(chauffeurs: BusChauffeur[]) {
        chauffeurs.forEach(ch => {
            this.bewerkteBusChauffeurs = this.bewerkteBusChauffeurs.filter(val => val !== ch);
        });
        this.geselecteerdeChauffeurs = [];
        this.bewerkteBusChauffeurs = _.clone(this.bewerkteBusChauffeurs);
    }

    compare() {
        return _.isEqual(this.bewerkteBusChauffeurs, this.verkregenBusChauffeurs);
    }

    getAllBuschauffeurs() {
        this.loadingBusChauffeurs = true;
        this.busChauffeurService.getAllBusChauffeurs$().subscribe(
            val => {
                if (val) {
                    this.bewerkteBusChauffeurs = val;
                    this.verkregenBusChauffeurs = _.cloneDeep(this.bewerkteBusChauffeurs);
                    this.loadingBusChauffeurs = false;
                }
            },
            error => {
                this.messageService.clear();
                if (error.error instanceof ProgressEvent) {
                    this.messageService.add({severity: 'error', summary: 'De databank is onbereikbaar!'});
                } else {
                    console.log(error);
                    this.messageService.add({severity: 'error', summary: error.error, life: 10000});
                }
                this.loadingBusChauffeurs = false;
            },
        );
    }

    getInstellingen() {
        this.instellingsService.getInstellingen$().subscribe(
            val => {
                if (val) {
                    this.instellingen = val;
                    this.geselecteerdeKolommen = this.instellingen.localBuschauffeurTabel;
                }
            },
            error => {
                this.messageService.clear();
                if (error.error instanceof ProgressEvent) {
                    this.messageService.add({severity: 'error', summary: 'De databank is onbereikbaar!'});
                } else {
                    console.log(error);
                    this.messageService.add({severity: 'error', summary: error.error, life: 10000});
                }
            },
        );
    }

    redirectTo(voorvoegsel: string, bc: any) {
        this.router.navigate([`../${voorvoegsel}/${bc.id}`]);
    }

    msToTime(s: number = 0): string {
        const ms = s % 1000;
        s = (s - ms) / 1000;
        const secs = s % 60;
        s = (s - secs) / 60;
        const mins = s % 60;
        const hrs = (s - mins) / 60;
        let uitvoer = '';
        if (s >= 0) {
            if (hrs.toString().length < 2) {
                uitvoer = `0${hrs}:`;
            } else {
                uitvoer = `${hrs}:`;
            }
            if (mins.toString().length < 2) {
                uitvoer += `0${mins}`;
            } else {
                uitvoer += `${mins}`;
            }
        } else {
            if (hrs.toString().length < 3) {
                uitvoer = `-0${Math.abs(hrs)}:`;
            } else {
                uitvoer = `${hrs}:`;
            }
            if (Math.abs(mins).toString().length < 2) {
                uitvoer += `0${Math.abs(mins)}`;
            } else {
                uitvoer += `${Math.abs(mins)}`;
            }
        }
        return uitvoer;
    }

    timeToMs(time: string, elementId?: string): number {
        var uitvoer;
        try {
            if (time == '' || time.match('[-]?[0-9]*:[0-5][0-9]$') == null) {
                uitvoer = 0;
            } else {
                const splittedTime = time.toString().split(':');
                if (time.substring(0, 1) != '-') {
                    uitvoer = 3600000 * parseInt(splittedTime[0]) + 60000 * parseInt(splittedTime[1]);
                } else {
                    uitvoer = 3600000 * parseInt(splittedTime[0]) - 60000 * parseInt(splittedTime[1]);
                }
            }
        } catch (e) {
            uitvoer = 0;
        }
        try {
            if (elementId != null) {
                (document.getElementById(elementId) as HTMLInputElement).value = this.msToTime(uitvoer);
            }
        } catch (e) {}
        return uitvoer;
    }
}
