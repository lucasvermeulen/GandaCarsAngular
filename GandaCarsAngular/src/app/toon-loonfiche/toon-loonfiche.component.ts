import {HttpErrorResponse} from '@angular/common/http';
import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import * as _ from 'lodash';
import * as moment from 'moment';
import {MessageService} from 'primeng/api';
import {forkJoin} from 'rxjs';
import * as XLSX from 'xlsx';
import {BusChauffeur} from '../modals/bus-chauffeur';
import {DagExport} from '../modals/dag-export';
import {Feestdag} from '../modals/feestdag';
import {LoonficheSetting} from '../modals/loonfiche-setting';
import {LoonficheTabel} from '../modals/loonfiche-tabel';
import {EffectieveDienstService} from '../services/effectieve-dienst.service';
import {FeestdagenService} from '../services/feestdagen.service';
import {LoonficheSettingService} from '../services/loonfiche-setting.service';

@Component({
    selector: 'app-toon-loonfiche',
    templateUrl: './toon-loonfiche.component.html',
    styleUrls: ['./toon-loonfiche.component.scss'],
})
export class ToonLoonficheComponent implements OnInit {
    public loonficheTabel: LoonficheTabel = new LoonficheTabel();
    public loading = false;
    public feestdagen: Feestdag[] = [];
    private dayNames = ['Zo', 'Ma', 'Di', 'Woe', 'Do', 'Vrij', 'Za'];
    public tabelVisible;
    constructor(
        private loonficheSettingService: LoonficheSettingService,
        private messageService: MessageService,
        private router: Router,
        private route: ActivatedRoute,
        public feestdagenService: FeestdagenService,
        public effectieveDienstenService: EffectieveDienstService,
    ) {
        this.route.data.subscribe(data => {
            this.loonficheTabel.maand = moment(new Date(this.route.snapshot.params.jaar, this.route.snapshot.params.maand - 1, 1));
            this.loonficheTabel.busChauffeur = data.busChauffeur;
            this.loonficheTabel.effectieveDiensten = data.effectieveDiensten;
            this.loonficheTabel.instellingen = data.instellingen;
            this.loonficheTabel.verkregenLoonficheSetting = _.cloneDeep(data.loonficheSetting);
            if (data.loonficheSetting == null) {
                this.tabelVisible = false;
                this.loonficheTabel.bewerkteLoonficheSetting = new LoonficheSetting(
                    this.loonficheTabel.maand.year(),
                    this.loonficheTabel.maand.month() + 1,
                    this.loonficheTabel.instellingen,
                    this.loonficheTabel.busChauffeur,
                );
            } else {
                this.tabelVisible = true;
                this.loonficheTabel.bewerkteLoonficheSetting = data.loonficheSetting;
            }
        });
    }

    ngOnInit() {
        this.loadFeestdagen();
    }

    compare() {
        return _.isEqual(this.loonficheTabel.bewerkteLoonficheSetting, this.loonficheTabel.verkregenLoonficheSetting);
    }

    loonficheSettingOpslaan() {
        this.loonficheTabel.bewerkteLoonficheSetting.resterendeUrenDezeMaand = this.loonficheTabel.resterendeUrenDezeMaand;
        this.loonficheTabel.bewerkteLoonficheSetting.gewerkteUrenDezeMaand = this.loonficheTabel.totaalDagExports.totaalAT;
        let request;
        if (this.loonficheTabel.bewerkteLoonficheSetting.id == null) {
            request = this.loonficheSettingService.addLoonficheSetting$(this.loonficheTabel.bewerkteLoonficheSetting);
        } else {
            request = this.loonficheSettingService.putLoonficheSetting$(this.loonficheTabel.bewerkteLoonficheSetting);
        }
        request.subscribe(
            val => {
                if (val) {
                    this.loonficheTabel.busChauffeur = BusChauffeur.fromJSON(val.busChauffeur);
                    this.loonficheTabel.bewerkteLoonficheSetting = val;
                    this.loonficheTabel.verkregenLoonficheSetting = _.cloneDeep(this.loonficheTabel.bewerkteLoonficheSetting);
                    this.messageService.add({
                        severity: 'success',
                        summary: 'Loonfiche opgeslaan!',
                    });
                    this.loonficheTabel.updateValues();
                }
            },
            error => {
                console.log(error);
                this.messageService.clear();
                if (error.error instanceof ProgressEvent) {
                    this.messageService.add({severity: 'error', summary: 'De databank is onbereikbaar!'});
                } else if (typeof error.error == 'string') {
                    this.messageService.add({severity: 'error', summary: error.error, life: 10000});
                } else if (typeof error.error == 'object') {
                    for (let key in error.error.errors) {
                        let errorToShow = error.error.errors[key][0];
                        if (errorToShow != null) {
                            this.messageService.add({severity: 'error', summary: errorToShow, life: 10000});
                        } else {
                            this.messageService.add({severity: 'error', summary: 'Er liep iets fout!', life: 10000});
                        }
                    }
                } else {
                    this.messageService.add({severity: 'error', summary: 'Er liep iets fout!', life: 10000});
                }
            },
        );
    }

    loadFeestdagen() {
        this.feestdagenService.getAllFeestdagen$().subscribe(
            val => {
                if (val) {
                    this.feestdagen = val;
                    this.dienstenToDagExports();
                    this.loading = false;
                }
            },
            error => {
                if (error.error instanceof ProgressEvent) {
                    this.messageService.add({
                        severity: 'error',
                        summary: 'De databank is onbereikbaar!',
                    });
                } else {
                    console.log(error);
                    this.messageService.add({
                        severity: 'error',
                        summary: error.error,
                        life: 10000,
                    });
                }
                this.loading = false;
            },
        );
    }

    redirectTo(voorvoegsel: string, bc: any) {
        this.router.navigate([`../${voorvoegsel}/${bc.id}`]);
    }

    printFiche() {
        // this.excelService.exportAsExcelFile(this.data, 'sample');
        const element = document.getElementById('afdrukken');
        const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);
        const wb: XLSX.WorkBook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
        XLSX.writeFile(
            wb,
            `${this.loonficheTabel.busChauffeur.voornaam}_${this.loonficheTabel.busChauffeur.achternaam}_${this.getHuidigeMaand()}_loonfiche.xlsx`,
        );
    }

    getHuidigeMaand() {
        return `${this.loonficheTabel.maand.format('MMMM').charAt(0).toUpperCase()}${this.loonficheTabel.maand
            .format('MMMM')
            .substring(1)} ${this.loonficheTabel.maand.year()}`;
    }

    veranderHuidigeMaand(met: number) {
        this.loading = true;
        if (met < 0) {
            this.loonficheTabel.maand.subtract(1, 'months');
        } else {
            this.loonficheTabel.maand.add(1, 'months');
        }
        forkJoin(
            this.effectieveDienstenService.getEffectieveDienstenByMonth$(
                this.loonficheTabel.maand.year(),
                this.loonficheTabel.maand.month() + 1,
                this.loonficheTabel.busChauffeur.id,
            ),
            this.loonficheSettingService.getLoonficheSettingByJaarEnMaand$(
                this.loonficheTabel.busChauffeur.id,
                this.loonficheTabel.maand.year(),
                this.loonficheTabel.maand.month() + 1,
            ),
        ).subscribe(
            val => {
                if (val) {
                    this.loonficheTabel.effectieveDiensten = val[0];
                    if (val[1] == null) {
                        this.loonficheTabel.bewerkteLoonficheSetting = new LoonficheSetting(
                            this.loonficheTabel.maand.year(),
                            this.loonficheTabel.maand.month() + 1,
                            this.loonficheTabel.instellingen,
                            this.loonficheTabel.busChauffeur,
                        );
                    } else {
                        this.loonficheTabel.bewerkteLoonficheSetting = val[1];
                    }
                    this.loonficheTabel.verkregenLoonficheSetting = _.cloneDeep(val[1]);
                    this.loading = false;
                    this.dienstenToDagExports();
                }
            },
            error => {
                if (error.error instanceof ProgressEvent) {
                    this.messageService.add({
                        severity: 'error',
                        summary: 'De databank is onbereikbaar!',
                    });
                } else {
                    console.log(error);
                    this.messageService.add({
                        severity: 'error',
                        summary: error.error,
                        life: 10000,
                    });
                }
                this.loading = false;
            },
        );
    }

    dienstenToDagExports() {
        const monthIndex = this.loonficheTabel.maand.month();
        const date = new Date(this.loonficheTabel.maand.year(), monthIndex, 1);
        const dagExports: DagExport[] = [];
        while (date.getMonth() === monthIndex) {
            const dagExport = new DagExport(
                date,
                this.loonficheTabel.maand.format('MM'),
                this.dayNames[date.getDay()],
                this.loonficheTabel.instellingen,
            );
            dagExports.push(dagExport);
            date.setDate(date.getDate() + 1);
        }
        this.loonficheTabel.effectieveDiensten.forEach(dienst => {
            if (dienst.gerelateerdeDienst === null || dienst.start < new Date(dienst.gerelateerdeDienst.start)) {
                const dienstenInExport = dagExports[dienst.start.getDate() - 1].effectieveDiensten.value;
                dienstenInExport.push(dienst);
                dagExports[dienst.start.getDate() - 1].effectieveDiensten.next(dienstenInExport);
            }
        });
        this.feestdagen
            .filter(x => x.dag.getMonth() == this.loonficheTabel.maand.month())
            .forEach(fd => {
                const dagExportVoorFeestdag = dagExports.find(x => x.dagNummer == fd.dag.getDate());
                const feestdagen = dagExportVoorFeestdag.feestdagen.value;
                feestdagen.push(fd);
                dagExportVoorFeestdag.feestdagen.next(feestdagen);
            });
        this.loonficheTabel.dagExports = dagExports;
        //bereken totaal
        const result = new DagExport(
            new Date(),
            this.loonficheTabel.maand.format('MM'),
            this.dayNames[new Date().getDay()],
            this.loonficheTabel.instellingen,
        );
        this.loonficheTabel.dagExports.forEach(basket => {
            for (const [key, value] of Object.entries(basket)) {
                if (result[key]) {
                    result[key] += value;
                } else {
                    result[key] = value;
                }
            }
        });
        this.loonficheTabel.totaalDagExports = result;
        this.loonficheTabel.updateValues();
    }

    deleteLoonficheSetting() {
        this.loading = true;
        this.loonficheSettingService.deleteLoonficheSetting$(this.loonficheTabel.verkregenLoonficheSetting).subscribe(
            val => {
                if (val) {
                    this.tabelVisible = false;
                    this.messageService.clear();
                    this.loonficheTabel.verkregenLoonficheSetting = null;
                    this.loonficheTabel.bewerkteLoonficheSetting = new LoonficheSetting(
                        this.loonficheTabel.maand.year(),
                        this.loonficheTabel.maand.month() + 1,
                        this.loonficheTabel.instellingen,
                        this.loonficheTabel.busChauffeur,
                    );
                    this.messageService.add({
                        severity: 'success',
                        summary: 'Loonfiche verwijderd!',
                    });
                    this.loading = false;
                }
            },
            (error: HttpErrorResponse) => {
                this.loading = true;
                this.messageService.clear();
                if (error.error instanceof ProgressEvent) {
                    this.messageService.add({severity: 'error', summary: 'De databank is onbereikbaar!'});
                } else {
                    console.log(error);
                    this.messageService.add({severity: 'error', summary: 'Er liep iets fout!', life: 10000});
                }
                this.loading = false;
            },
        );
    }

    changeTabelView() {
        if (!this.tabelVisible) {
            this.loonficheTabel.updateValues();
        }
        this.tabelVisible = !this.tabelVisible;
    }
}
