import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {BusChauffeurOverzichtComponent} from './bus-chauffeur-overzicht/bus-chauffeur-overzicht.component';
import {BuschauffeurInfoComponent} from './buschauffeur-info/buschauffeur-info.component';
import {DienstOverzichtComponent} from './dienst-overzicht/dienst-overzicht.component';
import {EffectieveWeekWijzigenComponent} from './effectieve-week-wijzigen/effectieve-week-wijzigen.component';
import {FeestdagenAanpassenComponent} from './feestdagen-aanpassen/feestdagen-aanpassen.component';
import {InstellingenAanpassenComponent} from './instellingen-aanpassen/instellingen-aanpassen.component';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {BusChauffeurResolver} from './resolvers/bus-chauffeur.resolver';
import {EffectieveDienstenByMonthResolver} from './resolvers/effectieve-diensten-by-month.resolver';
import {InstellingenResolver} from './resolvers/instellingen.resolver';
import {LoonficheSettingResolver} from './resolvers/loonfiche-setting.resolver';
import {ToonLoonficheComponent} from './toon-loonfiche/toon-loonfiche.component';
import {ToonLoonlijstComponent} from './toon-loonlijst/toon-loonlijst.component';

const routes: Routes = [
    {
        path: 'dienst-overzicht',
        component: DienstOverzichtComponent,
    },
    {
        path: 'buschauffeur-overzicht',
        component: BusChauffeurOverzichtComponent,
    },
    {
        path: 'buschauffeur-info/:id',
        component: BuschauffeurInfoComponent,
        resolve: {busChauffeur: BusChauffeurResolver},
    },
    {
        path: 'toon-loonlijst/:jaar/:maand/:id',
        component: ToonLoonlijstComponent,
        resolve: {
            busChauffeur: BusChauffeurResolver,
            effectieveDiensten: EffectieveDienstenByMonthResolver,
            instellingen: InstellingenResolver,
        },
    },
    {
        path: 'toon-loonfiche/:jaar/:maand/:id',
        component: ToonLoonficheComponent,
        resolve: {
            busChauffeur: BusChauffeurResolver,
            effectieveDiensten: EffectieveDienstenByMonthResolver,
            instellingen: InstellingenResolver,
            loonficheSetting: LoonficheSettingResolver,
        },
    },
    {
        path: 'feestdagen-aanpassen',
        component: FeestdagenAanpassenComponent,
    },
    {
        path: 'instellingen-aanpassen',
        component: InstellingenAanpassenComponent,
    },
    {
        path: 'effectieve-week-wijzigen/:jaar/:week/:id',
        component: EffectieveWeekWijzigenComponent,
        resolve: {
            busChauffeur: BusChauffeurResolver,
        },
    },
    {
        path: '',
        redirectTo: 'buschauffeur-overzicht',
        pathMatch: 'full',
    },
    {
        path: '**',
        component: PageNotFoundComponent,
    },
];

@NgModule({
    imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
    exports: [RouterModule],
})
export class AppRoutingModule {}
