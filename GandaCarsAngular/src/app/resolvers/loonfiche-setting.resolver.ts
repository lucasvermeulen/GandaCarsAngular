import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {LoonficheSetting} from '../modals/loonfiche-setting';
import {LoonficheSettingService} from '../services/loonfiche-setting.service';

@Injectable({
    providedIn: 'root',
})
export class LoonficheSettingResolver implements Resolve<LoonficheSetting> {
    constructor(private loonficheSettingService: LoonficheSettingService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<LoonficheSetting> {
        return this.loonficheSettingService.getLoonficheSettingByJaarEnMaand$(route.params.id, route.params.jaar, route.params.maand);
    }
}
