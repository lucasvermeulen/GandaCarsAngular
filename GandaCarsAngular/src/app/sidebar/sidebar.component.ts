import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {animateText, onMainContentChange, onSideNavChange} from '../animations/animations';

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss'],
    animations: [onSideNavChange, animateText, onMainContentChange],
})
export class SidebarComponent implements OnInit {
    public sidebarOpen: boolean = false;
    public linkText: boolean = false;

    constructor(public route: ActivatedRoute, public router: Router) {}

    ngOnInit() {}

    onSinenavToggle() {
        this.sidebarOpen = !this.sidebarOpen;
        if (this.sidebarOpen) {
            setTimeout(() => {
                this.linkText = this.sidebarOpen;
            }, 250);
        } else {
            this.linkText = this.sidebarOpen;
        }
    }
    closeOnNavigate() {
        if (this.sidebarOpen) {
            this.onSinenavToggle();
        }
    }
}
