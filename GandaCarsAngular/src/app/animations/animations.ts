import {animate, state, style, transition, trigger} from '@angular/animations';

export const onSideNavChange = trigger('onSideNavChange', [
    state(
        'close',
        style({
            'min-width': '80px',
        }),
    ),
    state(
        'open',
        style({
            'min-width': '220px',
        }),
    ),
    transition('close => open', animate('150ms ease-in')),
    transition('open => close', animate('150ms ease-in')),
]);

export const onMainContentChange = trigger('onMainContentChange', [
    state(
        'close',
        style({
            'margin-left': '80px',
        }),
    ),
    state(
        'open',
        style({
            'margin-left': '220px',
        }),
    ),
    transition('close => open', animate('150ms ease-in')),
    transition('open => close', animate('150ms ease-in')),
]);

export const animateText = trigger('animateText', [
    state(
        'hide',
        style({
            display: 'none',
            opacity: 0,
        }),
    ),
    state(
        'show',
        style({
            display: 'inline',
            opacity: 1,
        }),
    ),
    transition('close => open', animate('150ms ease-in')),
    transition('open => close', animate('150ms ease-out')),
]);
