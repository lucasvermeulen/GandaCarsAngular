import {CommonModule, DatePipe, registerLocaleData} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import localeBe from '@angular/common/locales/nl-BE';
import {APP_INITIALIZER, ErrorHandler, NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatSidenavModule} from '@angular/material/sidenav';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {Router} from '@angular/router';
import {FullCalendarModule} from '@fullcalendar/angular';
import * as Sentry from '@sentry/angular';
import * as moment from 'moment';
import {NgxMaskModule} from 'ngx-mask-2';
import {AutoCompleteModule} from 'primeng/autocomplete';
import {ButtonModule} from 'primeng/button';
import {CalendarModule} from 'primeng/calendar';
import {CardModule} from 'primeng/card';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {ContextMenuModule} from 'primeng/contextmenu';
import {DialogModule} from 'primeng/dialog';
import {DropdownModule} from 'primeng/dropdown';
import {DynamicDialogModule} from 'primeng/dynamicdialog';
import {FileUploadModule} from 'primeng/fileupload';
import {InputMaskModule} from 'primeng/inputmask';
import {InputNumberModule} from 'primeng/inputnumber';
import {InputSwitchModule} from 'primeng/inputswitch';
import {InputTextModule} from 'primeng/inputtext';
import {InputTextareaModule} from 'primeng/inputtextarea';
import {MessagesModule} from 'primeng/messages';
import {MultiSelectModule} from 'primeng/multiselect';
import {OverlayPanelModule} from 'primeng/overlaypanel';
import {PanelModule} from 'primeng/panel';
import {ProgressBarModule} from 'primeng/progressbar';
import {RadioButtonModule} from 'primeng/radiobutton';
import {RatingModule} from 'primeng/rating';
import {RippleModule} from 'primeng/ripple';
import {ScrollTopModule} from 'primeng/scrolltop';
import {SliderModule} from 'primeng/slider';
import {TableModule} from 'primeng/table';
import {TabMenuModule} from 'primeng/tabmenu';
import {TabViewModule} from 'primeng/tabview';
import {ToastModule} from 'primeng/toast';
import {ToolbarModule} from 'primeng/toolbar';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BusChauffeurOverzichtComponent} from './bus-chauffeur-overzicht/bus-chauffeur-overzicht.component';
import {BuschauffeurInfoComponent} from './buschauffeur-info/buschauffeur-info.component';
import {dienstKalenderComponent} from './dienst-kalender/dienst-kalender.component';
import {DienstOverzichtComponent} from './dienst-overzicht/dienst-overzicht.component';
import {DienstTypeOverzichtTabelComponent} from './dienst-type-overzicht-tabel/dienst-type-overzicht-tabel.component';
import {EffectieveWeekWijzigenComponent} from './effectieve-week-wijzigen/effectieve-week-wijzigen.component';
import {FeestdagenAanpassenComponent} from './feestdagen-aanpassen/feestdagen-aanpassen.component';
import {InstellingenAanpassenComponent} from './instellingen-aanpassen/instellingen-aanpassen.component';
import {LoonficheBewerkenComponent} from './loonfiche-bewerken/loonfiche-bewerken.component';
import {LoonficheTabelComponent} from './loonfiche-tabel/loonfiche-tabel.component';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {SidebarComponent} from './sidebar/sidebar.component';
import {ToonLoonficheComponent} from './toon-loonfiche/toon-loonfiche.component';
import {ToonLoonlijstComponent} from './toon-loonlijst/toon-loonlijst.component';
// the second parameter 'fr-FR' is optional
registerLocaleData(localeBe);
moment.locale('nl-be');
@NgModule({
    declarations: [
        AppComponent,
        SidebarComponent,
        BusChauffeurOverzichtComponent,
        BuschauffeurInfoComponent,
        dienstKalenderComponent,
        DienstOverzichtComponent,
        ToonLoonlijstComponent,
        FeestdagenAanpassenComponent,
        EffectieveWeekWijzigenComponent,
        PageNotFoundComponent,
        InstellingenAanpassenComponent,
        DienstTypeOverzichtTabelComponent,
        ToonLoonficheComponent,
        LoonficheTabelComponent,
        LoonficheBewerkenComponent,
    ],
    imports: [
        ScrollTopModule,
        OverlayPanelModule,
        MatSidenavModule,
        TabMenuModule,
        InputSwitchModule,
        InputMaskModule,
        BrowserModule,
        AppRoutingModule,
        TabViewModule,
        ReactiveFormsModule,
        HttpClientModule,
        FullCalendarModule,
        ToastModule,
        BrowserAnimationsModule,
        ButtonModule,
        RippleModule,
        ConfirmDialogModule,
        MessagesModule,
        NgxMaskModule.forRoot(),
        CommonModule,
        DynamicDialogModule,
        ToastModule,
        TableModule,
        ButtonModule,
        CalendarModule,
        SliderModule,
        DialogModule,
        MultiSelectModule,
        ContextMenuModule,
        DropdownModule,
        InputTextModule,
        ProgressBarModule,
        FileUploadModule,
        ToolbarModule,
        RatingModule,
        FormsModule,
        RadioButtonModule,
        InputNumberModule,
        InputTextareaModule,
        PanelModule,
        CardModule,
        AutoCompleteModule,
    ],
    providers: [
        DatePipe,
        {
            provide: ErrorHandler,
            useValue: Sentry.createErrorHandler({
                showDialog: true,
            }),
        },
        {
            provide: Sentry.TraceService,
            deps: [Router],
        },
        {
            provide: APP_INITIALIZER,
            useFactory: () => () => {},
            deps: [Sentry.TraceService],
            multi: true,
        },
    ],
    bootstrap: [AppComponent],
})
export class AppModule {}
