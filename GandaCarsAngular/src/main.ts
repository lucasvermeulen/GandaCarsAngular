import {enableProdMode} from '@angular/core';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import * as Sentry from '@sentry/angular';
import {Integrations} from '@sentry/tracing';
import {AppModule} from './app/app.module';
import {environment} from './environments/environment';

Sentry.init({
    enabled: environment.production,
    dsn: 'https://b5b548f054f44d0ebb0961e03b91c271@o462605.ingest.sentry.io/5466244',
    integrations: [
        new Integrations.BrowserTracing({
            tracingOrigins: ['localhost', 'localhost:5000', 'localhost:7000'],
            routingInstrumentation: Sentry.routingInstrumentation,
        }),
    ],
    tracesSampleRate: 1.0,
});

if (environment.production) {
    enableProdMode();
}
platformBrowserDynamic()
    .bootstrapModule(AppModule)
    .then(success => {})
    .catch(err => console.error(err));
