# base image
FROM node:12.2.0 as build

# set working directory
WORKDIR /app

# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH

# install and cache app dependencies
COPY ./GandaCarsAngular /app
RUN npm install
RUN npm install -g @angular/cli
RUN npm update @angular/core @angular/cli

# add app

# generate build
RUN ng build --output-path=dist --prod --aot --outputHashing=all

############
### prod ###
############

# base image
FROM nginx:1.16.0-alpine

COPY ./nginx.conf /etc/nginx/nginx.conf

# copy artifact build from the 'build environment'
COPY --from=build /app/dist /usr/share/nginx/html

# expose port 80
EXPOSE 4200 80
EXPOSE 433 433

# run nginx
CMD ["nginx", "-g", "daemon off;"]
